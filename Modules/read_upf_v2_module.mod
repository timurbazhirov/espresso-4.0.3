V26 read_upf_v2_module
15 read_upf_v2.F90 S582 0
04/24/2012  15:51:37
use parser private
use radial_grids private
use pseudo_types private
use kinds private
use parser private
use radial_grids private
use pseudo_types private
use kinds private
enduse
D 56 24 611 656 610 7
D 178 24 796 5360 795 7
D 184 18 2
S 582 24 0 0 0 9 1 0 4658 10015 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 0 0 0 0 0 0 read_upf_v2_module
S 584 23 0 0 0 9 594 582 4683 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 9 795 582 4699 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 pseudo_upf
S 588 23 0 0 0 9 610 582 4723 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 radial_grid_type
S 590 23 0 0 0 9 1187 582 4747 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 version_compare
R 594 16 1 kinds dp
R 610 25 3 radial_grids radial_grid_type
R 611 5 4 radial_grids mesh radial_grid_type
R 612 5 5 radial_grids r radial_grid_type
R 614 5 7 radial_grids r$sd radial_grid_type
R 615 5 8 radial_grids r$p radial_grid_type
R 616 5 9 radial_grids r$o radial_grid_type
R 618 5 11 radial_grids r2 radial_grid_type
R 620 5 13 radial_grids r2$sd radial_grid_type
R 621 5 14 radial_grids r2$p radial_grid_type
R 622 5 15 radial_grids r2$o radial_grid_type
R 624 5 17 radial_grids rab radial_grid_type
R 626 5 19 radial_grids rab$sd radial_grid_type
R 627 5 20 radial_grids rab$p radial_grid_type
R 628 5 21 radial_grids rab$o radial_grid_type
R 630 5 23 radial_grids sqr radial_grid_type
R 632 5 25 radial_grids sqr$sd radial_grid_type
R 633 5 26 radial_grids sqr$p radial_grid_type
R 634 5 27 radial_grids sqr$o radial_grid_type
R 636 5 29 radial_grids rm1 radial_grid_type
R 638 5 31 radial_grids rm1$sd radial_grid_type
R 639 5 32 radial_grids rm1$p radial_grid_type
R 640 5 33 radial_grids rm1$o radial_grid_type
R 642 5 35 radial_grids rm2 radial_grid_type
R 644 5 37 radial_grids rm2$sd radial_grid_type
R 645 5 38 radial_grids rm2$p radial_grid_type
R 646 5 39 radial_grids rm2$o radial_grid_type
R 648 5 41 radial_grids rm3 radial_grid_type
R 650 5 43 radial_grids rm3$sd radial_grid_type
R 651 5 44 radial_grids rm3$p radial_grid_type
R 652 5 45 radial_grids rm3$o radial_grid_type
R 654 5 47 radial_grids xmin radial_grid_type
R 655 5 48 radial_grids rmax radial_grid_type
R 656 5 49 radial_grids zmesh radial_grid_type
R 657 5 50 radial_grids dx radial_grid_type
S 735 3 0 0 0 20 1 1 0 0 0 A 0 0 0 0 0 0 0 0 5426 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 1 0
R 795 25 51 pseudo_types pseudo_upf
R 796 5 52 pseudo_types generated pseudo_upf
R 797 5 53 pseudo_types author pseudo_upf
R 798 5 54 pseudo_types date pseudo_upf
R 799 5 55 pseudo_types comment pseudo_upf
R 800 5 56 pseudo_types psd pseudo_upf
R 801 5 57 pseudo_types typ pseudo_upf
R 802 5 58 pseudo_types rel pseudo_upf
R 803 5 59 pseudo_types tvanp pseudo_upf
R 804 5 60 pseudo_types tcoulombp pseudo_upf
R 805 5 61 pseudo_types nlcc pseudo_upf
R 806 5 62 pseudo_types dft pseudo_upf
R 807 5 63 pseudo_types zp pseudo_upf
R 808 5 64 pseudo_types etotps pseudo_upf
R 809 5 65 pseudo_types ecutwfc pseudo_upf
R 810 5 66 pseudo_types ecutrho pseudo_upf
R 811 5 67 pseudo_types nv pseudo_upf
R 812 5 68 pseudo_types lmax pseudo_upf
R 813 5 69 pseudo_types lmax_rho pseudo_upf
R 814 5 70 pseudo_types nwfc pseudo_upf
R 815 5 71 pseudo_types nbeta pseudo_upf
R 816 5 72 pseudo_types kbeta pseudo_upf
R 818 5 74 pseudo_types kbeta$sd pseudo_upf
R 819 5 75 pseudo_types kbeta$p pseudo_upf
R 820 5 76 pseudo_types kbeta$o pseudo_upf
R 822 5 78 pseudo_types kkbeta pseudo_upf
R 823 5 79 pseudo_types lll pseudo_upf
R 825 5 81 pseudo_types lll$sd pseudo_upf
R 826 5 82 pseudo_types lll$p pseudo_upf
R 827 5 83 pseudo_types lll$o pseudo_upf
R 829 5 85 pseudo_types beta pseudo_upf
R 832 5 88 pseudo_types beta$sd pseudo_upf
R 833 5 89 pseudo_types beta$p pseudo_upf
R 834 5 90 pseudo_types beta$o pseudo_upf
R 836 5 92 pseudo_types els pseudo_upf
R 838 5 94 pseudo_types els$sd pseudo_upf
R 839 5 95 pseudo_types els$p pseudo_upf
R 840 5 96 pseudo_types els$o pseudo_upf
R 842 5 98 pseudo_types els_beta pseudo_upf
R 844 5 100 pseudo_types els_beta$sd pseudo_upf
R 845 5 101 pseudo_types els_beta$p pseudo_upf
R 846 5 102 pseudo_types els_beta$o pseudo_upf
R 848 5 104 pseudo_types nchi pseudo_upf
R 850 5 106 pseudo_types nchi$sd pseudo_upf
R 851 5 107 pseudo_types nchi$p pseudo_upf
R 852 5 108 pseudo_types nchi$o pseudo_upf
R 854 5 110 pseudo_types lchi pseudo_upf
R 856 5 112 pseudo_types lchi$sd pseudo_upf
R 857 5 113 pseudo_types lchi$p pseudo_upf
R 858 5 114 pseudo_types lchi$o pseudo_upf
R 861 5 117 pseudo_types oc pseudo_upf
R 862 5 118 pseudo_types oc$sd pseudo_upf
R 863 5 119 pseudo_types oc$p pseudo_upf
R 864 5 120 pseudo_types oc$o pseudo_upf
R 866 5 122 pseudo_types epseu pseudo_upf
R 868 5 124 pseudo_types epseu$sd pseudo_upf
R 869 5 125 pseudo_types epseu$p pseudo_upf
R 870 5 126 pseudo_types epseu$o pseudo_upf
R 872 5 128 pseudo_types rcut_chi pseudo_upf
R 874 5 130 pseudo_types rcut_chi$sd pseudo_upf
R 875 5 131 pseudo_types rcut_chi$p pseudo_upf
R 876 5 132 pseudo_types rcut_chi$o pseudo_upf
R 878 5 134 pseudo_types rcutus_chi pseudo_upf
R 880 5 136 pseudo_types rcutus_chi$sd pseudo_upf
R 881 5 137 pseudo_types rcutus_chi$p pseudo_upf
R 882 5 138 pseudo_types rcutus_chi$o pseudo_upf
R 884 5 140 pseudo_types chi pseudo_upf
R 887 5 143 pseudo_types chi$sd pseudo_upf
R 888 5 144 pseudo_types chi$p pseudo_upf
R 889 5 145 pseudo_types chi$o pseudo_upf
R 891 5 147 pseudo_types rho_at pseudo_upf
R 893 5 149 pseudo_types rho_at$sd pseudo_upf
R 894 5 150 pseudo_types rho_at$p pseudo_upf
R 895 5 151 pseudo_types rho_at$o pseudo_upf
R 897 5 153 pseudo_types mesh pseudo_upf
R 898 5 154 pseudo_types xmin pseudo_upf
R 899 5 155 pseudo_types rmax pseudo_upf
R 900 5 156 pseudo_types zmesh pseudo_upf
R 901 5 157 pseudo_types dx pseudo_upf
R 903 5 159 pseudo_types r pseudo_upf
R 904 5 160 pseudo_types r$sd pseudo_upf
R 905 5 161 pseudo_types r$p pseudo_upf
R 906 5 162 pseudo_types r$o pseudo_upf
R 909 5 165 pseudo_types rab pseudo_upf
R 910 5 166 pseudo_types rab$sd pseudo_upf
R 911 5 167 pseudo_types rab$p pseudo_upf
R 912 5 168 pseudo_types rab$o pseudo_upf
R 914 5 170 pseudo_types rho_atc pseudo_upf
R 916 5 172 pseudo_types rho_atc$sd pseudo_upf
R 917 5 173 pseudo_types rho_atc$p pseudo_upf
R 918 5 174 pseudo_types rho_atc$o pseudo_upf
R 920 5 176 pseudo_types lloc pseudo_upf
R 921 5 177 pseudo_types rcloc pseudo_upf
R 922 5 178 pseudo_types vloc pseudo_upf
R 924 5 180 pseudo_types vloc$sd pseudo_upf
R 925 5 181 pseudo_types vloc$p pseudo_upf
R 926 5 182 pseudo_types vloc$o pseudo_upf
R 928 5 184 pseudo_types dion pseudo_upf
R 931 5 187 pseudo_types dion$sd pseudo_upf
R 932 5 188 pseudo_types dion$p pseudo_upf
R 933 5 189 pseudo_types dion$o pseudo_upf
R 935 5 191 pseudo_types q_with_l pseudo_upf
R 936 5 192 pseudo_types nqf pseudo_upf
R 937 5 193 pseudo_types nqlc pseudo_upf
R 938 5 194 pseudo_types qqq_eps pseudo_upf
R 939 5 195 pseudo_types rinner pseudo_upf
R 941 5 197 pseudo_types rinner$sd pseudo_upf
R 942 5 198 pseudo_types rinner$p pseudo_upf
R 943 5 199 pseudo_types rinner$o pseudo_upf
R 945 5 201 pseudo_types qqq pseudo_upf
R 948 5 204 pseudo_types qqq$sd pseudo_upf
R 949 5 205 pseudo_types qqq$p pseudo_upf
R 950 5 206 pseudo_types qqq$o pseudo_upf
R 952 5 208 pseudo_types qfunc pseudo_upf
R 955 5 211 pseudo_types qfunc$sd pseudo_upf
R 956 5 212 pseudo_types qfunc$p pseudo_upf
R 957 5 213 pseudo_types qfunc$o pseudo_upf
R 959 5 215 pseudo_types qfuncl pseudo_upf
R 963 5 219 pseudo_types qfuncl$sd pseudo_upf
R 964 5 220 pseudo_types qfuncl$p pseudo_upf
R 965 5 221 pseudo_types qfuncl$o pseudo_upf
R 967 5 223 pseudo_types qfcoef pseudo_upf
R 972 5 228 pseudo_types qfcoef$sd pseudo_upf
R 973 5 229 pseudo_types qfcoef$p pseudo_upf
R 974 5 230 pseudo_types qfcoef$o pseudo_upf
R 976 5 232 pseudo_types has_wfc pseudo_upf
R 977 5 233 pseudo_types aewfc pseudo_upf
R 980 5 236 pseudo_types aewfc$sd pseudo_upf
R 981 5 237 pseudo_types aewfc$p pseudo_upf
R 982 5 238 pseudo_types aewfc$o pseudo_upf
R 984 5 240 pseudo_types pswfc pseudo_upf
R 987 5 243 pseudo_types pswfc$sd pseudo_upf
R 988 5 244 pseudo_types pswfc$p pseudo_upf
R 989 5 245 pseudo_types pswfc$o pseudo_upf
R 991 5 247 pseudo_types has_so pseudo_upf
R 992 5 248 pseudo_types nn pseudo_upf
R 994 5 250 pseudo_types nn$sd pseudo_upf
R 995 5 251 pseudo_types nn$p pseudo_upf
R 996 5 252 pseudo_types nn$o pseudo_upf
R 998 5 254 pseudo_types rcut pseudo_upf
R 1000 5 256 pseudo_types rcut$sd pseudo_upf
R 1001 5 257 pseudo_types rcut$p pseudo_upf
R 1002 5 258 pseudo_types rcut$o pseudo_upf
R 1004 5 260 pseudo_types rcutus pseudo_upf
R 1006 5 262 pseudo_types rcutus$sd pseudo_upf
R 1007 5 263 pseudo_types rcutus$p pseudo_upf
R 1008 5 264 pseudo_types rcutus$o pseudo_upf
R 1010 5 266 pseudo_types jchi pseudo_upf
R 1012 5 268 pseudo_types jchi$sd pseudo_upf
R 1013 5 269 pseudo_types jchi$p pseudo_upf
R 1014 5 270 pseudo_types jchi$o pseudo_upf
R 1016 5 272 pseudo_types jjj pseudo_upf
R 1018 5 274 pseudo_types jjj$sd pseudo_upf
R 1019 5 275 pseudo_types jjj$p pseudo_upf
R 1020 5 276 pseudo_types jjj$o pseudo_upf
R 1022 5 278 pseudo_types paw_data_format pseudo_upf
R 1023 5 279 pseudo_types tpawp pseudo_upf
R 1024 5 280 pseudo_types paw pseudo_upf
R 1025 5 281 pseudo_types grid pseudo_upf
R 1027 5 283 pseudo_types grid$p pseudo_upf
R 1029 5 285 pseudo_types has_gipaw pseudo_upf
R 1030 5 286 pseudo_types gipaw_data_format pseudo_upf
R 1031 5 287 pseudo_types gipaw_ncore_orbitals pseudo_upf
R 1032 5 288 pseudo_types gipaw_core_orbital_n pseudo_upf
R 1034 5 290 pseudo_types gipaw_core_orbital_n$sd pseudo_upf
R 1035 5 291 pseudo_types gipaw_core_orbital_n$p pseudo_upf
R 1036 5 292 pseudo_types gipaw_core_orbital_n$o pseudo_upf
R 1038 5 294 pseudo_types gipaw_core_orbital_l pseudo_upf
R 1040 5 296 pseudo_types gipaw_core_orbital_l$sd pseudo_upf
R 1041 5 297 pseudo_types gipaw_core_orbital_l$p pseudo_upf
R 1042 5 298 pseudo_types gipaw_core_orbital_l$o pseudo_upf
R 1044 5 300 pseudo_types gipaw_core_orbital_el pseudo_upf
R 1046 5 302 pseudo_types gipaw_core_orbital_el$sd pseudo_upf
R 1047 5 303 pseudo_types gipaw_core_orbital_el$p pseudo_upf
R 1048 5 304 pseudo_types gipaw_core_orbital_el$o pseudo_upf
R 1050 5 306 pseudo_types gipaw_core_orbital pseudo_upf
R 1053 5 309 pseudo_types gipaw_core_orbital$sd pseudo_upf
R 1054 5 310 pseudo_types gipaw_core_orbital$p pseudo_upf
R 1055 5 311 pseudo_types gipaw_core_orbital$o pseudo_upf
R 1057 5 313 pseudo_types gipaw_vlocal_ae pseudo_upf
R 1059 5 315 pseudo_types gipaw_vlocal_ae$sd pseudo_upf
R 1060 5 316 pseudo_types gipaw_vlocal_ae$p pseudo_upf
R 1061 5 317 pseudo_types gipaw_vlocal_ae$o pseudo_upf
R 1063 5 319 pseudo_types gipaw_vlocal_ps pseudo_upf
R 1065 5 321 pseudo_types gipaw_vlocal_ps$sd pseudo_upf
R 1066 5 322 pseudo_types gipaw_vlocal_ps$p pseudo_upf
R 1067 5 323 pseudo_types gipaw_vlocal_ps$o pseudo_upf
R 1069 5 325 pseudo_types gipaw_wfs_nchannels pseudo_upf
R 1070 5 326 pseudo_types gipaw_wfs_el pseudo_upf
R 1072 5 328 pseudo_types gipaw_wfs_el$sd pseudo_upf
R 1073 5 329 pseudo_types gipaw_wfs_el$p pseudo_upf
R 1074 5 330 pseudo_types gipaw_wfs_el$o pseudo_upf
R 1076 5 332 pseudo_types gipaw_wfs_ll pseudo_upf
R 1078 5 334 pseudo_types gipaw_wfs_ll$sd pseudo_upf
R 1079 5 335 pseudo_types gipaw_wfs_ll$p pseudo_upf
R 1080 5 336 pseudo_types gipaw_wfs_ll$o pseudo_upf
R 1082 5 338 pseudo_types gipaw_wfs_ae pseudo_upf
R 1085 5 341 pseudo_types gipaw_wfs_ae$sd pseudo_upf
R 1086 5 342 pseudo_types gipaw_wfs_ae$p pseudo_upf
R 1087 5 343 pseudo_types gipaw_wfs_ae$o pseudo_upf
R 1089 5 345 pseudo_types gipaw_wfs_rcut pseudo_upf
R 1091 5 347 pseudo_types gipaw_wfs_rcut$sd pseudo_upf
R 1092 5 348 pseudo_types gipaw_wfs_rcut$p pseudo_upf
R 1093 5 349 pseudo_types gipaw_wfs_rcut$o pseudo_upf
R 1095 5 351 pseudo_types gipaw_wfs_rcutus pseudo_upf
R 1097 5 353 pseudo_types gipaw_wfs_rcutus$sd pseudo_upf
R 1098 5 354 pseudo_types gipaw_wfs_rcutus$p pseudo_upf
R 1099 5 355 pseudo_types gipaw_wfs_rcutus$o pseudo_upf
R 1101 5 357 pseudo_types gipaw_wfs_ps pseudo_upf
R 1104 5 360 pseudo_types gipaw_wfs_ps$sd pseudo_upf
R 1105 5 361 pseudo_types gipaw_wfs_ps$p pseudo_upf
R 1106 5 362 pseudo_types gipaw_wfs_ps$o pseudo_upf
R 1187 14 40 parser version_compare
S 3533 6 4 0 0 9 1 582 19122 0 1000 A 0 0 0 0 0 0 0 0 0 0 0 0 3534 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 read_upf_v2
S 3534 11 0 0 0 9 1310 582 19134 40800000 801000 A 0 0 0 0 0 8 0 0 3533 3533 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 _read_upf_v2_module$2
S 3535 23 5 0 0 0 3540 582 19122 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_upf_v2
S 3536 1 3 1 0 6 1 3535 19156 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 u
S 3537 1 3 3 0 178 1 3535 9032 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 3538 1 3 3 0 56 1 3535 5357 80000014 3008 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 grid
S 3539 1 3 2 0 6 1 3535 9397 80000014 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ierr
S 3540 14 5 0 0 0 1 3535 19122 0 400000 A 0 0 0 0 0 0 0 1746 4 0 0 0 0 0 0 0 0 0 0 0 0 27 0 582 0 0 0 0 read_upf_v2
F 3540 4 3536 3537 3538 3539
A 593 2 0 0 137 184 735 0 0 0 593 0 0 0 0 0 0 0 0 0
Z
T 795 178 0 3 0 0
A 796 184 0 0 1 593 1
A 797 184 0 0 1 593 1
A 798 184 0 0 1 593 1
A 799 184 0 0 1 593 1
A 800 184 0 0 1 593 1
A 801 184 0 0 1 593 1
A 802 184 0 0 1 593 0
Z
