V26 path_opt_routines
21 path_opt_routines.F90 S582 0
04/24/2012  15:51:13
use mp private
use io_global private
use path_variables private
use constants private
use kinds private
use mp private
use io_global private
use path_variables private
use constants private
use kinds private
enduse
S 582 24 0 0 0 9 1 0 4658 10015 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 0 0 0 0 0 0 path_opt_routines
S 584 23 0 0 0 9 600 582 4682 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 9 683 582 4695 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 eps8
S 587 23 0 0 0 9 686 582 4700 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 eps16
S 589 23 0 0 0 9 720 582 4721 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 ds
S 590 23 0 0 0 9 744 582 4724 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 pos
S 591 23 0 0 0 9 808 582 4728 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 grad
S 593 23 0 0 0 6 836 582 4743 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 meta_ionode
S 594 23 0 0 0 6 837 582 4755 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 meta_ionode_id
S 596 19 0 0 0 6 1 582 4773 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 146 20 0 0 0 0 0 582 0 0 0 0 mp_bcast
O 596 20 1610 1595 1548 1530 1642 1633 1515 1474 1462 1583 1503 1628 1574 1494 1453 1659 1654 1569 1489 1448
R 600 16 1 kinds dp
R 683 16 35 constants eps8
R 686 16 38 constants eps16
R 720 6 12 path_variables ds
R 744 7 36 path_variables pos
R 808 7 100 path_variables grad
R 836 6 7 io_global meta_ionode
R 837 6 8 io_global meta_ionode_id
R 1448 14 162 mp mp_bcast_i1
R 1453 14 167 mp mp_bcast_iv
R 1462 14 176 mp mp_bcast_im
R 1474 14 188 mp mp_bcast_it
R 1489 14 203 mp mp_bcast_r1
R 1494 14 208 mp mp_bcast_rv
R 1503 14 217 mp mp_bcast_rm
R 1515 14 229 mp mp_bcast_rt
R 1530 14 244 mp mp_bcast_r4d
R 1548 14 262 mp mp_bcast_r5d
R 1569 14 283 mp mp_bcast_c1
R 1574 14 288 mp mp_bcast_cv
R 1583 14 297 mp mp_bcast_cm
R 1595 14 309 mp mp_bcast_ct
R 1610 14 324 mp mp_bcast_c4d
R 1628 14 342 mp mp_bcast_l
R 1633 14 347 mp mp_bcast_lv
R 1642 14 356 mp mp_bcast_lm
R 1654 14 368 mp mp_bcast_z
R 1659 14 373 mp mp_bcast_zv
S 2263 27 0 0 0 6 2267 582 13117 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 langevin
S 2264 27 0 0 0 9 2270 582 13126 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 steepest_descent
S 2265 27 0 0 0 9 2273 582 13143 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 quick_min
S 2266 27 0 0 0 9 2277 582 13153 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 broyden
S 2267 23 5 0 0 0 2269 582 13117 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 langevin
S 2268 1 3 1 0 6 1 2267 13161 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 idx
S 2269 14 5 0 0 0 1 2267 13117 0 400000 A 0 0 0 0 0 0 0 326 1 0 0 0 0 0 0 0 0 0 0 0 0 36 0 582 0 0 0 0 langevin
F 2269 1 2268
S 2270 23 5 0 0 0 2272 582 13126 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 steepest_descent
S 2271 1 3 1 0 6 1 2270 13161 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 idx
S 2272 14 5 0 0 0 1 2270 13126 0 400000 A 0 0 0 0 0 0 0 328 1 0 0 0 0 0 0 0 0 0 0 0 0 58 0 582 0 0 0 0 steepest_descent
F 2272 1 2271
S 2273 23 5 0 0 0 2276 582 13143 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 quick_min
S 2274 1 3 1 0 6 1 2273 13161 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 idx
S 2275 1 3 1 0 6 1 2273 13165 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 istep
S 2276 14 5 0 0 0 1 2273 13143 0 400000 A 0 0 0 0 0 0 0 330 2 0 0 0 0 0 0 0 0 0 0 0 0 78 0 582 0 0 0 0 quick_min
F 2276 2 2274 2275
S 2277 23 5 0 0 0 2278 582 13153 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 broyden
S 2278 14 5 0 0 0 1 2277 13153 0 400000 A 0 0 0 0 0 0 0 333 0 0 0 0 0 0 0 0 0 0 0 0 0 143 0 582 0 0 0 0 broyden
F 2278 0
Z
Z
