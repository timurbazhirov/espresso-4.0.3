V26 uspp_param
8 uspp.F90 S582 0
04/24/2012  15:51:15
use pseudo_types private
use parameters private
use kinds private
use pseudo_types private
use parameters private
use kinds private
enduse
D 178 24 808 5360 807 7
D 184 18 2
D 459 21 178 1 616 615 0 1 0 0 1
 610 613 614 610 613 611
D 462 21 6 1 0 36 0 0 0 0 0
 0 36 0 3 36 0
D 465 21 6 1 3 18 0 0 0 0 0
 0 18 3 3 18 18
D 468 21 6 2 15 110 0 0 0 0 0
 0 23 3 3 23 23
 0 18 23 3 18 18
D 471 21 16 1 3 18 0 0 0 0 0
 0 18 3 3 18 18
D 474 21 16 1 3 18 0 0 0 0 0
 0 18 3 3 18 18
S 582 24 0 0 0 9 1 0 4658 5 0 A 0 0 0 0 0 0 0 0 0 0 0 0 34 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 uspp_param
S 584 23 0 0 0 9 591 582 4675 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 6 605 582 4689 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 npsx
S 588 23 0 0 0 9 807 582 4707 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 pseudo_upf
S 589 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 590 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 591 16 1 kinds dp
S 598 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 10 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 600 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 602 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 605 16 2 parameters npsx
S 617 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 18 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 618 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 13 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 619 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 620 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 740 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 30 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 748 3 0 0 0 184 1 1 0 0 0 A 0 0 0 0 0 0 0 0 5460 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 0
R 807 25 51 pseudo_types pseudo_upf
R 808 5 52 pseudo_types generated pseudo_upf
R 809 5 53 pseudo_types author pseudo_upf
R 810 5 54 pseudo_types date pseudo_upf
R 811 5 55 pseudo_types comment pseudo_upf
R 812 5 56 pseudo_types psd pseudo_upf
R 813 5 57 pseudo_types typ pseudo_upf
R 814 5 58 pseudo_types rel pseudo_upf
R 815 5 59 pseudo_types tvanp pseudo_upf
R 816 5 60 pseudo_types tcoulombp pseudo_upf
R 817 5 61 pseudo_types nlcc pseudo_upf
R 818 5 62 pseudo_types dft pseudo_upf
R 819 5 63 pseudo_types zp pseudo_upf
R 820 5 64 pseudo_types etotps pseudo_upf
R 821 5 65 pseudo_types ecutwfc pseudo_upf
R 822 5 66 pseudo_types ecutrho pseudo_upf
R 823 5 67 pseudo_types nv pseudo_upf
R 824 5 68 pseudo_types lmax pseudo_upf
R 825 5 69 pseudo_types lmax_rho pseudo_upf
R 826 5 70 pseudo_types nwfc pseudo_upf
R 827 5 71 pseudo_types nbeta pseudo_upf
R 828 5 72 pseudo_types kbeta pseudo_upf
R 830 5 74 pseudo_types kbeta$sd pseudo_upf
R 831 5 75 pseudo_types kbeta$p pseudo_upf
R 832 5 76 pseudo_types kbeta$o pseudo_upf
R 834 5 78 pseudo_types kkbeta pseudo_upf
R 835 5 79 pseudo_types lll pseudo_upf
R 837 5 81 pseudo_types lll$sd pseudo_upf
R 838 5 82 pseudo_types lll$p pseudo_upf
R 839 5 83 pseudo_types lll$o pseudo_upf
R 841 5 85 pseudo_types beta pseudo_upf
R 844 5 88 pseudo_types beta$sd pseudo_upf
R 845 5 89 pseudo_types beta$p pseudo_upf
R 846 5 90 pseudo_types beta$o pseudo_upf
R 848 5 92 pseudo_types els pseudo_upf
R 850 5 94 pseudo_types els$sd pseudo_upf
R 851 5 95 pseudo_types els$p pseudo_upf
R 852 5 96 pseudo_types els$o pseudo_upf
R 854 5 98 pseudo_types els_beta pseudo_upf
R 856 5 100 pseudo_types els_beta$sd pseudo_upf
R 857 5 101 pseudo_types els_beta$p pseudo_upf
R 858 5 102 pseudo_types els_beta$o pseudo_upf
R 860 5 104 pseudo_types nchi pseudo_upf
R 862 5 106 pseudo_types nchi$sd pseudo_upf
R 863 5 107 pseudo_types nchi$p pseudo_upf
R 864 5 108 pseudo_types nchi$o pseudo_upf
R 866 5 110 pseudo_types lchi pseudo_upf
R 868 5 112 pseudo_types lchi$sd pseudo_upf
R 869 5 113 pseudo_types lchi$p pseudo_upf
R 870 5 114 pseudo_types lchi$o pseudo_upf
R 873 5 117 pseudo_types oc pseudo_upf
R 874 5 118 pseudo_types oc$sd pseudo_upf
R 875 5 119 pseudo_types oc$p pseudo_upf
R 876 5 120 pseudo_types oc$o pseudo_upf
R 878 5 122 pseudo_types epseu pseudo_upf
R 880 5 124 pseudo_types epseu$sd pseudo_upf
R 881 5 125 pseudo_types epseu$p pseudo_upf
R 882 5 126 pseudo_types epseu$o pseudo_upf
R 884 5 128 pseudo_types rcut_chi pseudo_upf
R 886 5 130 pseudo_types rcut_chi$sd pseudo_upf
R 887 5 131 pseudo_types rcut_chi$p pseudo_upf
R 888 5 132 pseudo_types rcut_chi$o pseudo_upf
R 890 5 134 pseudo_types rcutus_chi pseudo_upf
R 892 5 136 pseudo_types rcutus_chi$sd pseudo_upf
R 893 5 137 pseudo_types rcutus_chi$p pseudo_upf
R 894 5 138 pseudo_types rcutus_chi$o pseudo_upf
R 896 5 140 pseudo_types chi pseudo_upf
R 899 5 143 pseudo_types chi$sd pseudo_upf
R 900 5 144 pseudo_types chi$p pseudo_upf
R 901 5 145 pseudo_types chi$o pseudo_upf
R 903 5 147 pseudo_types rho_at pseudo_upf
R 905 5 149 pseudo_types rho_at$sd pseudo_upf
R 906 5 150 pseudo_types rho_at$p pseudo_upf
R 907 5 151 pseudo_types rho_at$o pseudo_upf
R 909 5 153 pseudo_types mesh pseudo_upf
R 910 5 154 pseudo_types xmin pseudo_upf
R 911 5 155 pseudo_types rmax pseudo_upf
R 912 5 156 pseudo_types zmesh pseudo_upf
R 913 5 157 pseudo_types dx pseudo_upf
R 915 5 159 pseudo_types r pseudo_upf
R 916 5 160 pseudo_types r$sd pseudo_upf
R 917 5 161 pseudo_types r$p pseudo_upf
R 918 5 162 pseudo_types r$o pseudo_upf
R 921 5 165 pseudo_types rab pseudo_upf
R 922 5 166 pseudo_types rab$sd pseudo_upf
R 923 5 167 pseudo_types rab$p pseudo_upf
R 924 5 168 pseudo_types rab$o pseudo_upf
R 926 5 170 pseudo_types rho_atc pseudo_upf
R 928 5 172 pseudo_types rho_atc$sd pseudo_upf
R 929 5 173 pseudo_types rho_atc$p pseudo_upf
R 930 5 174 pseudo_types rho_atc$o pseudo_upf
R 932 5 176 pseudo_types lloc pseudo_upf
R 933 5 177 pseudo_types rcloc pseudo_upf
R 934 5 178 pseudo_types vloc pseudo_upf
R 936 5 180 pseudo_types vloc$sd pseudo_upf
R 937 5 181 pseudo_types vloc$p pseudo_upf
R 938 5 182 pseudo_types vloc$o pseudo_upf
R 940 5 184 pseudo_types dion pseudo_upf
R 943 5 187 pseudo_types dion$sd pseudo_upf
R 944 5 188 pseudo_types dion$p pseudo_upf
R 945 5 189 pseudo_types dion$o pseudo_upf
R 947 5 191 pseudo_types q_with_l pseudo_upf
R 948 5 192 pseudo_types nqf pseudo_upf
R 949 5 193 pseudo_types nqlc pseudo_upf
R 950 5 194 pseudo_types qqq_eps pseudo_upf
R 951 5 195 pseudo_types rinner pseudo_upf
R 953 5 197 pseudo_types rinner$sd pseudo_upf
R 954 5 198 pseudo_types rinner$p pseudo_upf
R 955 5 199 pseudo_types rinner$o pseudo_upf
R 957 5 201 pseudo_types qqq pseudo_upf
R 960 5 204 pseudo_types qqq$sd pseudo_upf
R 961 5 205 pseudo_types qqq$p pseudo_upf
R 962 5 206 pseudo_types qqq$o pseudo_upf
R 964 5 208 pseudo_types qfunc pseudo_upf
R 967 5 211 pseudo_types qfunc$sd pseudo_upf
R 968 5 212 pseudo_types qfunc$p pseudo_upf
R 969 5 213 pseudo_types qfunc$o pseudo_upf
R 971 5 215 pseudo_types qfuncl pseudo_upf
R 975 5 219 pseudo_types qfuncl$sd pseudo_upf
R 976 5 220 pseudo_types qfuncl$p pseudo_upf
R 977 5 221 pseudo_types qfuncl$o pseudo_upf
R 979 5 223 pseudo_types qfcoef pseudo_upf
R 984 5 228 pseudo_types qfcoef$sd pseudo_upf
R 985 5 229 pseudo_types qfcoef$p pseudo_upf
R 986 5 230 pseudo_types qfcoef$o pseudo_upf
R 988 5 232 pseudo_types has_wfc pseudo_upf
R 989 5 233 pseudo_types aewfc pseudo_upf
R 992 5 236 pseudo_types aewfc$sd pseudo_upf
R 993 5 237 pseudo_types aewfc$p pseudo_upf
R 994 5 238 pseudo_types aewfc$o pseudo_upf
R 996 5 240 pseudo_types pswfc pseudo_upf
R 999 5 243 pseudo_types pswfc$sd pseudo_upf
R 1000 5 244 pseudo_types pswfc$p pseudo_upf
R 1001 5 245 pseudo_types pswfc$o pseudo_upf
R 1003 5 247 pseudo_types has_so pseudo_upf
R 1004 5 248 pseudo_types nn pseudo_upf
R 1006 5 250 pseudo_types nn$sd pseudo_upf
R 1007 5 251 pseudo_types nn$p pseudo_upf
R 1008 5 252 pseudo_types nn$o pseudo_upf
R 1010 5 254 pseudo_types rcut pseudo_upf
R 1012 5 256 pseudo_types rcut$sd pseudo_upf
R 1013 5 257 pseudo_types rcut$p pseudo_upf
R 1014 5 258 pseudo_types rcut$o pseudo_upf
R 1016 5 260 pseudo_types rcutus pseudo_upf
R 1018 5 262 pseudo_types rcutus$sd pseudo_upf
R 1019 5 263 pseudo_types rcutus$p pseudo_upf
R 1020 5 264 pseudo_types rcutus$o pseudo_upf
R 1022 5 266 pseudo_types jchi pseudo_upf
R 1024 5 268 pseudo_types jchi$sd pseudo_upf
R 1025 5 269 pseudo_types jchi$p pseudo_upf
R 1026 5 270 pseudo_types jchi$o pseudo_upf
R 1028 5 272 pseudo_types jjj pseudo_upf
R 1030 5 274 pseudo_types jjj$sd pseudo_upf
R 1031 5 275 pseudo_types jjj$p pseudo_upf
R 1032 5 276 pseudo_types jjj$o pseudo_upf
R 1034 5 278 pseudo_types paw_data_format pseudo_upf
R 1035 5 279 pseudo_types tpawp pseudo_upf
R 1036 5 280 pseudo_types paw pseudo_upf
R 1037 5 281 pseudo_types grid pseudo_upf
R 1039 5 283 pseudo_types grid$p pseudo_upf
R 1041 5 285 pseudo_types has_gipaw pseudo_upf
R 1042 5 286 pseudo_types gipaw_data_format pseudo_upf
R 1043 5 287 pseudo_types gipaw_ncore_orbitals pseudo_upf
R 1044 5 288 pseudo_types gipaw_core_orbital_n pseudo_upf
R 1046 5 290 pseudo_types gipaw_core_orbital_n$sd pseudo_upf
R 1047 5 291 pseudo_types gipaw_core_orbital_n$p pseudo_upf
R 1048 5 292 pseudo_types gipaw_core_orbital_n$o pseudo_upf
R 1050 5 294 pseudo_types gipaw_core_orbital_l pseudo_upf
R 1052 5 296 pseudo_types gipaw_core_orbital_l$sd pseudo_upf
R 1053 5 297 pseudo_types gipaw_core_orbital_l$p pseudo_upf
R 1054 5 298 pseudo_types gipaw_core_orbital_l$o pseudo_upf
R 1056 5 300 pseudo_types gipaw_core_orbital_el pseudo_upf
R 1058 5 302 pseudo_types gipaw_core_orbital_el$sd pseudo_upf
R 1059 5 303 pseudo_types gipaw_core_orbital_el$p pseudo_upf
R 1060 5 304 pseudo_types gipaw_core_orbital_el$o pseudo_upf
R 1062 5 306 pseudo_types gipaw_core_orbital pseudo_upf
R 1065 5 309 pseudo_types gipaw_core_orbital$sd pseudo_upf
R 1066 5 310 pseudo_types gipaw_core_orbital$p pseudo_upf
R 1067 5 311 pseudo_types gipaw_core_orbital$o pseudo_upf
R 1069 5 313 pseudo_types gipaw_vlocal_ae pseudo_upf
R 1071 5 315 pseudo_types gipaw_vlocal_ae$sd pseudo_upf
R 1072 5 316 pseudo_types gipaw_vlocal_ae$p pseudo_upf
R 1073 5 317 pseudo_types gipaw_vlocal_ae$o pseudo_upf
R 1075 5 319 pseudo_types gipaw_vlocal_ps pseudo_upf
R 1077 5 321 pseudo_types gipaw_vlocal_ps$sd pseudo_upf
R 1078 5 322 pseudo_types gipaw_vlocal_ps$p pseudo_upf
R 1079 5 323 pseudo_types gipaw_vlocal_ps$o pseudo_upf
R 1081 5 325 pseudo_types gipaw_wfs_nchannels pseudo_upf
R 1082 5 326 pseudo_types gipaw_wfs_el pseudo_upf
R 1084 5 328 pseudo_types gipaw_wfs_el$sd pseudo_upf
R 1085 5 329 pseudo_types gipaw_wfs_el$p pseudo_upf
R 1086 5 330 pseudo_types gipaw_wfs_el$o pseudo_upf
R 1088 5 332 pseudo_types gipaw_wfs_ll pseudo_upf
R 1090 5 334 pseudo_types gipaw_wfs_ll$sd pseudo_upf
R 1091 5 335 pseudo_types gipaw_wfs_ll$p pseudo_upf
R 1092 5 336 pseudo_types gipaw_wfs_ll$o pseudo_upf
R 1094 5 338 pseudo_types gipaw_wfs_ae pseudo_upf
R 1097 5 341 pseudo_types gipaw_wfs_ae$sd pseudo_upf
R 1098 5 342 pseudo_types gipaw_wfs_ae$p pseudo_upf
R 1099 5 343 pseudo_types gipaw_wfs_ae$o pseudo_upf
R 1101 5 345 pseudo_types gipaw_wfs_rcut pseudo_upf
R 1103 5 347 pseudo_types gipaw_wfs_rcut$sd pseudo_upf
R 1104 5 348 pseudo_types gipaw_wfs_rcut$p pseudo_upf
R 1105 5 349 pseudo_types gipaw_wfs_rcut$o pseudo_upf
R 1107 5 351 pseudo_types gipaw_wfs_rcutus pseudo_upf
R 1109 5 353 pseudo_types gipaw_wfs_rcutus$sd pseudo_upf
R 1110 5 354 pseudo_types gipaw_wfs_rcutus$p pseudo_upf
R 1111 5 355 pseudo_types gipaw_wfs_rcutus$o pseudo_upf
R 1113 5 357 pseudo_types gipaw_wfs_ps pseudo_upf
R 1116 5 360 pseudo_types gipaw_wfs_ps$sd pseudo_upf
R 1117 5 361 pseudo_types gipaw_wfs_ps$p pseudo_upf
R 1118 5 362 pseudo_types gipaw_wfs_ps$o pseudo_upf
S 1135 7 6 0 0 459 1 582 9066 10a00004 59 A 0 0 0 0 0 0 1138 0 0 0 1140 0 0 0 0 0 0 0 0 1137 0 0 1139 582 0 0 0 0 upf
S 1136 6 4 0 0 6 1141 582 9092 40800006 0 A 0 0 0 0 0 0 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 z_b_0_2
S 1137 8 4 0 0 462 1136 582 9100 40822064 1020 A 0 0 0 0 0 0 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 upf$sd
S 1138 6 4 0 0 7 1139 582 9107 40802061 1020 A 0 0 0 0 0 72 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 upf$p
S 1139 6 4 0 0 7 1137 582 9113 40802060 1020 A 0 0 0 0 0 80 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 upf$o
S 1140 22 1 0 0 9 1 582 9119 40000000 1000 A 0 0 0 0 0 0 0 1135 0 0 0 0 1137 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 upf$arrdsc
S 1141 7 4 0 4 465 1142 582 9130 800004 100 A 0 0 0 0 0 16 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 nh
S 1142 6 4 0 0 6 1143 582 9133 4 0 A 0 0 0 0 0 56 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 nhm
S 1143 6 4 0 0 6 1144 582 9137 4 0 A 0 0 0 0 0 60 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 nbetam
S 1144 7 4 0 4 468 1145 582 9144 800004 100 A 0 0 0 0 0 64 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 iver
S 1145 6 4 0 0 6 1146 582 9149 4 0 A 0 0 0 0 0 184 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 lmaxkb
S 1146 6 4 0 0 6 1147 582 9156 4 0 A 0 0 0 0 0 188 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 lmaxq
S 1147 7 4 0 4 471 1148 582 9162 800004 100 A 0 0 0 0 0 192 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 newpseudo
S 1148 7 4 0 4 474 1 582 9172 800004 100 A 0 0 0 0 0 240 0 0 0 0 0 0 1149 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 oldvan
S 1149 11 0 0 4 9 1122 582 9179 40800000 801000 A 0 0 0 0 0 368 0 0 1138 1148 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 _uspp_param$0
A 13 2 0 0 0 6 589 0 0 0 13 0 0 0 0 0 0 0 0 0
A 15 2 0 0 0 6 590 0 0 0 15 0 0 0 0 0 0 0 0 0
A 18 2 0 0 0 6 598 0 0 0 18 0 0 0 0 0 0 0 0 0
A 23 2 0 0 0 6 600 0 0 0 23 0 0 0 0 0 0 0 0 0
A 25 2 0 0 0 6 602 0 0 0 25 0 0 0 0 0 0 0 0 0
A 36 2 0 0 0 6 617 0 0 0 36 0 0 0 0 0 0 0 0 0
A 39 2 0 0 0 6 618 0 0 0 39 0 0 0 0 0 0 0 0 0
A 41 2 0 0 0 6 619 0 0 0 41 0 0 0 0 0 0 0 0 0
A 45 2 0 0 0 6 620 0 0 0 45 0 0 0 0 0 0 0 0 0
A 110 2 0 0 0 6 740 0 0 0 110 0 0 0 0 0 0 0 0 0
A 607 2 0 0 0 184 748 0 0 0 607 0 0 0 0 0 0 0 0 0
A 609 1 0 1 77 462 1137 0 0 0 0 0 0 0 0 0 0 0 0 0
A 610 10 0 0 28 6 609 4 0 0 0 0 0 0 0 0 0 0 0 0
X 1 39
A 611 10 0 0 610 6 609 7 0 0 0 0 0 0 0 0 0 0 0 0
X 1 41
A 612 4 0 0 0 6 611 0 3 0 0 0 0 2 0 0 0 0 0 0
A 613 4 0 0 603 6 610 0 612 0 0 0 0 1 0 0 0 0 0 0
A 614 10 0 0 611 6 609 10 0 0 0 0 0 0 0 0 0 0 0 0
X 1 45
A 615 10 0 0 614 6 609 13 0 0 0 0 0 0 0 0 0 0 0 0
X 1 25
A 616 10 0 0 615 6 609 1 0 0 0 0 0 0 0 0 0 0 0 0
X 1 13
Z
T 807 178 0 3 0 0
A 808 184 0 0 1 607 1
A 809 184 0 0 1 607 1
A 810 184 0 0 1 607 1
A 811 184 0 0 1 607 1
A 812 184 0 0 1 607 1
A 813 184 0 0 1 607 1
A 814 184 0 0 1 607 0
Z
