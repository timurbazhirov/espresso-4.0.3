V26 read_uspp_module
13 read_uspp.F90 S582 0
04/24/2012  15:51:21
use pseudo_types private
use uspp_param private
use funct private
use io_global private
use parameters private
use kinds private
use pseudo_types private
use uspp_param private
use funct private
use io_global private
use parameters private
use kinds private
enduse
D 203 24 1037 5360 1036 7
D 209 18 2
D 490 18 2
D 510 21 9 1 3 627 0 0 1 0 0
 0 626 3 3 627 627
D 513 21 9 1 3 627 0 0 1 0 0
 0 626 3 3 627 627
S 582 24 0 0 0 9 1 0 4658 10015 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 0 0 0 0 0 0 read_uspp_module
S 584 23 0 0 0 9 599 582 4681 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 6 615 582 4695 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 lmaxx
S 587 23 0 0 0 6 616 582 4701 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 lqmax
S 589 23 0 0 0 9 626 582 4717 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 stdout
S 591 23 0 0 0 9 706 582 4730 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 set_dft_from_name
S 592 23 0 0 0 9 748 582 4748 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dft_is_hybrid
S 593 23 0 0 0 9 745 582 4762 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dft_is_meta
S 594 23 0 0 0 9 755 582 4774 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 set_dft_from_indices
S 596 23 0 0 0 9 1380 582 4806 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 oldvan
R 599 16 1 kinds dp
R 615 16 4 parameters lmaxx
R 616 16 5 parameters lqmax
R 626 6 4 io_global stdout
R 706 14 55 funct set_dft_from_name
R 745 14 94 funct dft_is_meta
R 748 14 97 funct dft_is_hybrid
R 755 14 104 funct set_dft_from_indices
S 978 3 0 0 0 490 1 1 0 0 0 A 0 0 0 0 0 0 0 0 6473 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 0
R 1036 25 51 pseudo_types pseudo_upf
R 1037 5 52 pseudo_types generated pseudo_upf
R 1038 5 53 pseudo_types author pseudo_upf
R 1039 5 54 pseudo_types date pseudo_upf
R 1040 5 55 pseudo_types comment pseudo_upf
R 1041 5 56 pseudo_types psd pseudo_upf
R 1042 5 57 pseudo_types typ pseudo_upf
R 1043 5 58 pseudo_types rel pseudo_upf
R 1044 5 59 pseudo_types tvanp pseudo_upf
R 1045 5 60 pseudo_types tcoulombp pseudo_upf
R 1046 5 61 pseudo_types nlcc pseudo_upf
R 1047 5 62 pseudo_types dft pseudo_upf
R 1048 5 63 pseudo_types zp pseudo_upf
R 1049 5 64 pseudo_types etotps pseudo_upf
R 1050 5 65 pseudo_types ecutwfc pseudo_upf
R 1051 5 66 pseudo_types ecutrho pseudo_upf
R 1052 5 67 pseudo_types nv pseudo_upf
R 1053 5 68 pseudo_types lmax pseudo_upf
R 1054 5 69 pseudo_types lmax_rho pseudo_upf
R 1055 5 70 pseudo_types nwfc pseudo_upf
R 1056 5 71 pseudo_types nbeta pseudo_upf
R 1057 5 72 pseudo_types kbeta pseudo_upf
R 1059 5 74 pseudo_types kbeta$sd pseudo_upf
R 1060 5 75 pseudo_types kbeta$p pseudo_upf
R 1061 5 76 pseudo_types kbeta$o pseudo_upf
R 1063 5 78 pseudo_types kkbeta pseudo_upf
R 1064 5 79 pseudo_types lll pseudo_upf
R 1066 5 81 pseudo_types lll$sd pseudo_upf
R 1067 5 82 pseudo_types lll$p pseudo_upf
R 1068 5 83 pseudo_types lll$o pseudo_upf
R 1070 5 85 pseudo_types beta pseudo_upf
R 1073 5 88 pseudo_types beta$sd pseudo_upf
R 1074 5 89 pseudo_types beta$p pseudo_upf
R 1075 5 90 pseudo_types beta$o pseudo_upf
R 1077 5 92 pseudo_types els pseudo_upf
R 1079 5 94 pseudo_types els$sd pseudo_upf
R 1080 5 95 pseudo_types els$p pseudo_upf
R 1081 5 96 pseudo_types els$o pseudo_upf
R 1083 5 98 pseudo_types els_beta pseudo_upf
R 1085 5 100 pseudo_types els_beta$sd pseudo_upf
R 1086 5 101 pseudo_types els_beta$p pseudo_upf
R 1087 5 102 pseudo_types els_beta$o pseudo_upf
R 1089 5 104 pseudo_types nchi pseudo_upf
R 1091 5 106 pseudo_types nchi$sd pseudo_upf
R 1092 5 107 pseudo_types nchi$p pseudo_upf
R 1093 5 108 pseudo_types nchi$o pseudo_upf
R 1095 5 110 pseudo_types lchi pseudo_upf
R 1097 5 112 pseudo_types lchi$sd pseudo_upf
R 1098 5 113 pseudo_types lchi$p pseudo_upf
R 1099 5 114 pseudo_types lchi$o pseudo_upf
R 1102 5 117 pseudo_types oc pseudo_upf
R 1103 5 118 pseudo_types oc$sd pseudo_upf
R 1104 5 119 pseudo_types oc$p pseudo_upf
R 1105 5 120 pseudo_types oc$o pseudo_upf
R 1107 5 122 pseudo_types epseu pseudo_upf
R 1109 5 124 pseudo_types epseu$sd pseudo_upf
R 1110 5 125 pseudo_types epseu$p pseudo_upf
R 1111 5 126 pseudo_types epseu$o pseudo_upf
R 1113 5 128 pseudo_types rcut_chi pseudo_upf
R 1115 5 130 pseudo_types rcut_chi$sd pseudo_upf
R 1116 5 131 pseudo_types rcut_chi$p pseudo_upf
R 1117 5 132 pseudo_types rcut_chi$o pseudo_upf
R 1119 5 134 pseudo_types rcutus_chi pseudo_upf
R 1121 5 136 pseudo_types rcutus_chi$sd pseudo_upf
R 1122 5 137 pseudo_types rcutus_chi$p pseudo_upf
R 1123 5 138 pseudo_types rcutus_chi$o pseudo_upf
R 1125 5 140 pseudo_types chi pseudo_upf
R 1128 5 143 pseudo_types chi$sd pseudo_upf
R 1129 5 144 pseudo_types chi$p pseudo_upf
R 1130 5 145 pseudo_types chi$o pseudo_upf
R 1132 5 147 pseudo_types rho_at pseudo_upf
R 1134 5 149 pseudo_types rho_at$sd pseudo_upf
R 1135 5 150 pseudo_types rho_at$p pseudo_upf
R 1136 5 151 pseudo_types rho_at$o pseudo_upf
R 1138 5 153 pseudo_types mesh pseudo_upf
R 1139 5 154 pseudo_types xmin pseudo_upf
R 1140 5 155 pseudo_types rmax pseudo_upf
R 1141 5 156 pseudo_types zmesh pseudo_upf
R 1142 5 157 pseudo_types dx pseudo_upf
R 1144 5 159 pseudo_types r pseudo_upf
R 1145 5 160 pseudo_types r$sd pseudo_upf
R 1146 5 161 pseudo_types r$p pseudo_upf
R 1147 5 162 pseudo_types r$o pseudo_upf
R 1150 5 165 pseudo_types rab pseudo_upf
R 1151 5 166 pseudo_types rab$sd pseudo_upf
R 1152 5 167 pseudo_types rab$p pseudo_upf
R 1153 5 168 pseudo_types rab$o pseudo_upf
R 1155 5 170 pseudo_types rho_atc pseudo_upf
R 1157 5 172 pseudo_types rho_atc$sd pseudo_upf
R 1158 5 173 pseudo_types rho_atc$p pseudo_upf
R 1159 5 174 pseudo_types rho_atc$o pseudo_upf
R 1161 5 176 pseudo_types lloc pseudo_upf
R 1162 5 177 pseudo_types rcloc pseudo_upf
R 1163 5 178 pseudo_types vloc pseudo_upf
R 1165 5 180 pseudo_types vloc$sd pseudo_upf
R 1166 5 181 pseudo_types vloc$p pseudo_upf
R 1167 5 182 pseudo_types vloc$o pseudo_upf
R 1169 5 184 pseudo_types dion pseudo_upf
R 1172 5 187 pseudo_types dion$sd pseudo_upf
R 1173 5 188 pseudo_types dion$p pseudo_upf
R 1174 5 189 pseudo_types dion$o pseudo_upf
R 1176 5 191 pseudo_types q_with_l pseudo_upf
R 1177 5 192 pseudo_types nqf pseudo_upf
R 1178 5 193 pseudo_types nqlc pseudo_upf
R 1179 5 194 pseudo_types qqq_eps pseudo_upf
R 1180 5 195 pseudo_types rinner pseudo_upf
R 1182 5 197 pseudo_types rinner$sd pseudo_upf
R 1183 5 198 pseudo_types rinner$p pseudo_upf
R 1184 5 199 pseudo_types rinner$o pseudo_upf
R 1186 5 201 pseudo_types qqq pseudo_upf
R 1189 5 204 pseudo_types qqq$sd pseudo_upf
R 1190 5 205 pseudo_types qqq$p pseudo_upf
R 1191 5 206 pseudo_types qqq$o pseudo_upf
R 1193 5 208 pseudo_types qfunc pseudo_upf
R 1196 5 211 pseudo_types qfunc$sd pseudo_upf
R 1197 5 212 pseudo_types qfunc$p pseudo_upf
R 1198 5 213 pseudo_types qfunc$o pseudo_upf
R 1200 5 215 pseudo_types qfuncl pseudo_upf
R 1204 5 219 pseudo_types qfuncl$sd pseudo_upf
R 1205 5 220 pseudo_types qfuncl$p pseudo_upf
R 1206 5 221 pseudo_types qfuncl$o pseudo_upf
R 1208 5 223 pseudo_types qfcoef pseudo_upf
R 1213 5 228 pseudo_types qfcoef$sd pseudo_upf
R 1214 5 229 pseudo_types qfcoef$p pseudo_upf
R 1215 5 230 pseudo_types qfcoef$o pseudo_upf
R 1217 5 232 pseudo_types has_wfc pseudo_upf
R 1218 5 233 pseudo_types aewfc pseudo_upf
R 1221 5 236 pseudo_types aewfc$sd pseudo_upf
R 1222 5 237 pseudo_types aewfc$p pseudo_upf
R 1223 5 238 pseudo_types aewfc$o pseudo_upf
R 1225 5 240 pseudo_types pswfc pseudo_upf
R 1228 5 243 pseudo_types pswfc$sd pseudo_upf
R 1229 5 244 pseudo_types pswfc$p pseudo_upf
R 1230 5 245 pseudo_types pswfc$o pseudo_upf
R 1232 5 247 pseudo_types has_so pseudo_upf
R 1233 5 248 pseudo_types nn pseudo_upf
R 1235 5 250 pseudo_types nn$sd pseudo_upf
R 1236 5 251 pseudo_types nn$p pseudo_upf
R 1237 5 252 pseudo_types nn$o pseudo_upf
R 1239 5 254 pseudo_types rcut pseudo_upf
R 1241 5 256 pseudo_types rcut$sd pseudo_upf
R 1242 5 257 pseudo_types rcut$p pseudo_upf
R 1243 5 258 pseudo_types rcut$o pseudo_upf
R 1245 5 260 pseudo_types rcutus pseudo_upf
R 1247 5 262 pseudo_types rcutus$sd pseudo_upf
R 1248 5 263 pseudo_types rcutus$p pseudo_upf
R 1249 5 264 pseudo_types rcutus$o pseudo_upf
R 1251 5 266 pseudo_types jchi pseudo_upf
R 1253 5 268 pseudo_types jchi$sd pseudo_upf
R 1254 5 269 pseudo_types jchi$p pseudo_upf
R 1255 5 270 pseudo_types jchi$o pseudo_upf
R 1257 5 272 pseudo_types jjj pseudo_upf
R 1259 5 274 pseudo_types jjj$sd pseudo_upf
R 1260 5 275 pseudo_types jjj$p pseudo_upf
R 1261 5 276 pseudo_types jjj$o pseudo_upf
R 1263 5 278 pseudo_types paw_data_format pseudo_upf
R 1264 5 279 pseudo_types tpawp pseudo_upf
R 1265 5 280 pseudo_types paw pseudo_upf
R 1266 5 281 pseudo_types grid pseudo_upf
R 1268 5 283 pseudo_types grid$p pseudo_upf
R 1270 5 285 pseudo_types has_gipaw pseudo_upf
R 1271 5 286 pseudo_types gipaw_data_format pseudo_upf
R 1272 5 287 pseudo_types gipaw_ncore_orbitals pseudo_upf
R 1273 5 288 pseudo_types gipaw_core_orbital_n pseudo_upf
R 1275 5 290 pseudo_types gipaw_core_orbital_n$sd pseudo_upf
R 1276 5 291 pseudo_types gipaw_core_orbital_n$p pseudo_upf
R 1277 5 292 pseudo_types gipaw_core_orbital_n$o pseudo_upf
R 1279 5 294 pseudo_types gipaw_core_orbital_l pseudo_upf
R 1281 5 296 pseudo_types gipaw_core_orbital_l$sd pseudo_upf
R 1282 5 297 pseudo_types gipaw_core_orbital_l$p pseudo_upf
R 1283 5 298 pseudo_types gipaw_core_orbital_l$o pseudo_upf
R 1285 5 300 pseudo_types gipaw_core_orbital_el pseudo_upf
R 1287 5 302 pseudo_types gipaw_core_orbital_el$sd pseudo_upf
R 1288 5 303 pseudo_types gipaw_core_orbital_el$p pseudo_upf
R 1289 5 304 pseudo_types gipaw_core_orbital_el$o pseudo_upf
R 1291 5 306 pseudo_types gipaw_core_orbital pseudo_upf
R 1294 5 309 pseudo_types gipaw_core_orbital$sd pseudo_upf
R 1295 5 310 pseudo_types gipaw_core_orbital$p pseudo_upf
R 1296 5 311 pseudo_types gipaw_core_orbital$o pseudo_upf
R 1298 5 313 pseudo_types gipaw_vlocal_ae pseudo_upf
R 1300 5 315 pseudo_types gipaw_vlocal_ae$sd pseudo_upf
R 1301 5 316 pseudo_types gipaw_vlocal_ae$p pseudo_upf
R 1302 5 317 pseudo_types gipaw_vlocal_ae$o pseudo_upf
R 1304 5 319 pseudo_types gipaw_vlocal_ps pseudo_upf
R 1306 5 321 pseudo_types gipaw_vlocal_ps$sd pseudo_upf
R 1307 5 322 pseudo_types gipaw_vlocal_ps$p pseudo_upf
R 1308 5 323 pseudo_types gipaw_vlocal_ps$o pseudo_upf
R 1310 5 325 pseudo_types gipaw_wfs_nchannels pseudo_upf
R 1311 5 326 pseudo_types gipaw_wfs_el pseudo_upf
R 1313 5 328 pseudo_types gipaw_wfs_el$sd pseudo_upf
R 1314 5 329 pseudo_types gipaw_wfs_el$p pseudo_upf
R 1315 5 330 pseudo_types gipaw_wfs_el$o pseudo_upf
R 1317 5 332 pseudo_types gipaw_wfs_ll pseudo_upf
R 1319 5 334 pseudo_types gipaw_wfs_ll$sd pseudo_upf
R 1320 5 335 pseudo_types gipaw_wfs_ll$p pseudo_upf
R 1321 5 336 pseudo_types gipaw_wfs_ll$o pseudo_upf
R 1323 5 338 pseudo_types gipaw_wfs_ae pseudo_upf
R 1326 5 341 pseudo_types gipaw_wfs_ae$sd pseudo_upf
R 1327 5 342 pseudo_types gipaw_wfs_ae$p pseudo_upf
R 1328 5 343 pseudo_types gipaw_wfs_ae$o pseudo_upf
R 1330 5 345 pseudo_types gipaw_wfs_rcut pseudo_upf
R 1332 5 347 pseudo_types gipaw_wfs_rcut$sd pseudo_upf
R 1333 5 348 pseudo_types gipaw_wfs_rcut$p pseudo_upf
R 1334 5 349 pseudo_types gipaw_wfs_rcut$o pseudo_upf
R 1336 5 351 pseudo_types gipaw_wfs_rcutus pseudo_upf
R 1338 5 353 pseudo_types gipaw_wfs_rcutus$sd pseudo_upf
R 1339 5 354 pseudo_types gipaw_wfs_rcutus$p pseudo_upf
R 1340 5 355 pseudo_types gipaw_wfs_rcutus$o pseudo_upf
R 1342 5 357 pseudo_types gipaw_wfs_ps pseudo_upf
R 1345 5 360 pseudo_types gipaw_wfs_ps$sd pseudo_upf
R 1346 5 361 pseudo_types gipaw_wfs_ps$p pseudo_upf
R 1347 5 362 pseudo_types gipaw_wfs_ps$o pseudo_upf
R 1380 7 17 uspp_param oldvan
S 1382 27 0 0 0 9 1384 582 10206 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 readvan
S 1383 27 0 0 0 9 1396 582 10214 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 readrrkj
S 1384 23 5 0 0 0 1388 582 10206 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 readvan
S 1385 1 3 0 0 6 1 1384 10223 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1386 1 3 0 0 6 1 1384 10229 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 is
S 1387 1 3 0 0 203 1 1384 10086 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1388 14 5 0 0 0 1 1384 10206 0 400000 A 0 0 0 0 0 0 0 162 3 0 0 0 0 0 0 0 0 0 0 0 0 34 0 582 0 0 0 0 readvan
F 1388 3 1385 1386 1387
S 1389 23 5 0 0 0 1394 582 10232 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 herman_skillman_grid
S 1390 6 3 0 0 6 1 1389 5845 800014 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mesh
S 1391 1 3 0 0 9 1 1389 10253 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z
S 1392 7 3 0 0 510 1 1389 5850 800214 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 r
S 1393 7 3 0 0 513 1 1389 5915 800214 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rab
S 1394 14 5 0 0 0 1 1389 10232 210 400000 A 0 0 0 0 0 0 0 166 4 0 0 0 0 0 0 0 0 0 0 0 0 564 0 582 0 0 0 0 herman_skillman_grid
F 1394 4 1390 1391 1392 1393
S 1395 6 1 0 0 6 1 1389 10255 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_626
S 1396 23 5 0 0 0 1400 582 10214 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 readrrkj
S 1397 1 3 0 0 6 1 1396 10223 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1398 1 3 0 0 6 1 1396 10229 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 is
S 1399 1 3 0 0 203 1 1396 10086 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1400 14 5 0 0 0 1 1396 10214 0 400000 A 0 0 0 0 0 0 0 171 3 0 0 0 0 0 0 0 0 0 0 0 0 596 0 582 0 0 0 0 readrrkj
F 1400 3 1397 1398 1399
A 616 2 0 0 0 209 978 0 0 0 616 0 0 0 0 0 0 0 0 0
A 626 1 0 0 0 6 1390 0 0 0 0 0 0 0 0 0 0 0 0 0
A 627 1 0 0 563 6 1395 0 0 0 0 0 0 0 0 0 0 0 0 0
Z
T 1036 203 0 3 0 0
A 1037 209 0 0 1 616 1
A 1038 209 0 0 1 616 1
A 1039 209 0 0 1 616 1
A 1040 209 0 0 1 616 1
A 1041 209 0 0 1 616 1
A 1042 209 0 0 1 616 1
A 1043 209 0 0 1 616 0
Z
