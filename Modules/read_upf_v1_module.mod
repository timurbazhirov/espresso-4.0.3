V26 read_upf_v1_module
15 read_upf_v1.F90 S582 0
04/24/2012  15:51:21
use pseudo_types private
use radial_grids private
use kinds private
use pseudo_types private
use radial_grids private
use kinds private
enduse
D 56 24 606 656 605 7
D 178 24 795 5360 794 7
D 184 18 2
D 459 24 795 5360 794 7
S 582 24 0 0 0 9 1 0 4658 10015 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 12 0 0 0 0 0 0 read_upf_v1_module
S 584 23 0 0 0 9 589 582 4683 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 9 674 582 4699 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 allocate_radial_grid
R 589 16 1 kinds dp
R 605 25 3 radial_grids radial_grid_type
R 606 5 4 radial_grids mesh radial_grid_type
R 607 5 5 radial_grids r radial_grid_type
R 609 5 7 radial_grids r$sd radial_grid_type
R 610 5 8 radial_grids r$p radial_grid_type
R 611 5 9 radial_grids r$o radial_grid_type
R 613 5 11 radial_grids r2 radial_grid_type
R 615 5 13 radial_grids r2$sd radial_grid_type
R 616 5 14 radial_grids r2$p radial_grid_type
R 617 5 15 radial_grids r2$o radial_grid_type
R 619 5 17 radial_grids rab radial_grid_type
R 621 5 19 radial_grids rab$sd radial_grid_type
R 622 5 20 radial_grids rab$p radial_grid_type
R 623 5 21 radial_grids rab$o radial_grid_type
R 625 5 23 radial_grids sqr radial_grid_type
R 627 5 25 radial_grids sqr$sd radial_grid_type
R 628 5 26 radial_grids sqr$p radial_grid_type
R 629 5 27 radial_grids sqr$o radial_grid_type
R 631 5 29 radial_grids rm1 radial_grid_type
R 633 5 31 radial_grids rm1$sd radial_grid_type
R 634 5 32 radial_grids rm1$p radial_grid_type
R 635 5 33 radial_grids rm1$o radial_grid_type
R 637 5 35 radial_grids rm2 radial_grid_type
R 639 5 37 radial_grids rm2$sd radial_grid_type
R 640 5 38 radial_grids rm2$p radial_grid_type
R 641 5 39 radial_grids rm2$o radial_grid_type
R 643 5 41 radial_grids rm3 radial_grid_type
R 645 5 43 radial_grids rm3$sd radial_grid_type
R 646 5 44 radial_grids rm3$p radial_grid_type
R 647 5 45 radial_grids rm3$o radial_grid_type
R 649 5 47 radial_grids xmin radial_grid_type
R 650 5 48 radial_grids rmax radial_grid_type
R 651 5 49 radial_grids zmesh radial_grid_type
R 652 5 50 radial_grids dx radial_grid_type
R 674 14 72 radial_grids allocate_radial_grid
S 722 27 0 0 0 9 1122 582 5367 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 read_upf_v1
S 723 27 0 0 0 9 1129 582 5379 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 scan_begin
S 724 27 0 0 0 9 1134 582 5390 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 scan_end
S 734 3 0 0 0 184 1 1 0 0 0 A 0 0 0 0 0 0 0 0 5412 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 0
R 794 25 51 pseudo_types pseudo_upf
R 795 5 52 pseudo_types generated pseudo_upf
R 796 5 53 pseudo_types author pseudo_upf
R 797 5 54 pseudo_types date pseudo_upf
R 798 5 55 pseudo_types comment pseudo_upf
R 799 5 56 pseudo_types psd pseudo_upf
R 800 5 57 pseudo_types typ pseudo_upf
R 801 5 58 pseudo_types rel pseudo_upf
R 802 5 59 pseudo_types tvanp pseudo_upf
R 803 5 60 pseudo_types tcoulombp pseudo_upf
R 804 5 61 pseudo_types nlcc pseudo_upf
R 805 5 62 pseudo_types dft pseudo_upf
R 806 5 63 pseudo_types zp pseudo_upf
R 807 5 64 pseudo_types etotps pseudo_upf
R 808 5 65 pseudo_types ecutwfc pseudo_upf
R 809 5 66 pseudo_types ecutrho pseudo_upf
R 810 5 67 pseudo_types nv pseudo_upf
R 811 5 68 pseudo_types lmax pseudo_upf
R 812 5 69 pseudo_types lmax_rho pseudo_upf
R 813 5 70 pseudo_types nwfc pseudo_upf
R 814 5 71 pseudo_types nbeta pseudo_upf
R 815 5 72 pseudo_types kbeta pseudo_upf
R 817 5 74 pseudo_types kbeta$sd pseudo_upf
R 818 5 75 pseudo_types kbeta$p pseudo_upf
R 819 5 76 pseudo_types kbeta$o pseudo_upf
R 821 5 78 pseudo_types kkbeta pseudo_upf
R 822 5 79 pseudo_types lll pseudo_upf
R 824 5 81 pseudo_types lll$sd pseudo_upf
R 825 5 82 pseudo_types lll$p pseudo_upf
R 826 5 83 pseudo_types lll$o pseudo_upf
R 828 5 85 pseudo_types beta pseudo_upf
R 831 5 88 pseudo_types beta$sd pseudo_upf
R 832 5 89 pseudo_types beta$p pseudo_upf
R 833 5 90 pseudo_types beta$o pseudo_upf
R 835 5 92 pseudo_types els pseudo_upf
R 837 5 94 pseudo_types els$sd pseudo_upf
R 838 5 95 pseudo_types els$p pseudo_upf
R 839 5 96 pseudo_types els$o pseudo_upf
R 841 5 98 pseudo_types els_beta pseudo_upf
R 843 5 100 pseudo_types els_beta$sd pseudo_upf
R 844 5 101 pseudo_types els_beta$p pseudo_upf
R 845 5 102 pseudo_types els_beta$o pseudo_upf
R 847 5 104 pseudo_types nchi pseudo_upf
R 849 5 106 pseudo_types nchi$sd pseudo_upf
R 850 5 107 pseudo_types nchi$p pseudo_upf
R 851 5 108 pseudo_types nchi$o pseudo_upf
R 853 5 110 pseudo_types lchi pseudo_upf
R 855 5 112 pseudo_types lchi$sd pseudo_upf
R 856 5 113 pseudo_types lchi$p pseudo_upf
R 857 5 114 pseudo_types lchi$o pseudo_upf
R 860 5 117 pseudo_types oc pseudo_upf
R 861 5 118 pseudo_types oc$sd pseudo_upf
R 862 5 119 pseudo_types oc$p pseudo_upf
R 863 5 120 pseudo_types oc$o pseudo_upf
R 865 5 122 pseudo_types epseu pseudo_upf
R 867 5 124 pseudo_types epseu$sd pseudo_upf
R 868 5 125 pseudo_types epseu$p pseudo_upf
R 869 5 126 pseudo_types epseu$o pseudo_upf
R 871 5 128 pseudo_types rcut_chi pseudo_upf
R 873 5 130 pseudo_types rcut_chi$sd pseudo_upf
R 874 5 131 pseudo_types rcut_chi$p pseudo_upf
R 875 5 132 pseudo_types rcut_chi$o pseudo_upf
R 877 5 134 pseudo_types rcutus_chi pseudo_upf
R 879 5 136 pseudo_types rcutus_chi$sd pseudo_upf
R 880 5 137 pseudo_types rcutus_chi$p pseudo_upf
R 881 5 138 pseudo_types rcutus_chi$o pseudo_upf
R 883 5 140 pseudo_types chi pseudo_upf
R 886 5 143 pseudo_types chi$sd pseudo_upf
R 887 5 144 pseudo_types chi$p pseudo_upf
R 888 5 145 pseudo_types chi$o pseudo_upf
R 890 5 147 pseudo_types rho_at pseudo_upf
R 892 5 149 pseudo_types rho_at$sd pseudo_upf
R 893 5 150 pseudo_types rho_at$p pseudo_upf
R 894 5 151 pseudo_types rho_at$o pseudo_upf
R 896 5 153 pseudo_types mesh pseudo_upf
R 897 5 154 pseudo_types xmin pseudo_upf
R 898 5 155 pseudo_types rmax pseudo_upf
R 899 5 156 pseudo_types zmesh pseudo_upf
R 900 5 157 pseudo_types dx pseudo_upf
R 902 5 159 pseudo_types r pseudo_upf
R 903 5 160 pseudo_types r$sd pseudo_upf
R 904 5 161 pseudo_types r$p pseudo_upf
R 905 5 162 pseudo_types r$o pseudo_upf
R 908 5 165 pseudo_types rab pseudo_upf
R 909 5 166 pseudo_types rab$sd pseudo_upf
R 910 5 167 pseudo_types rab$p pseudo_upf
R 911 5 168 pseudo_types rab$o pseudo_upf
R 913 5 170 pseudo_types rho_atc pseudo_upf
R 915 5 172 pseudo_types rho_atc$sd pseudo_upf
R 916 5 173 pseudo_types rho_atc$p pseudo_upf
R 917 5 174 pseudo_types rho_atc$o pseudo_upf
R 919 5 176 pseudo_types lloc pseudo_upf
R 920 5 177 pseudo_types rcloc pseudo_upf
R 921 5 178 pseudo_types vloc pseudo_upf
R 923 5 180 pseudo_types vloc$sd pseudo_upf
R 924 5 181 pseudo_types vloc$p pseudo_upf
R 925 5 182 pseudo_types vloc$o pseudo_upf
R 927 5 184 pseudo_types dion pseudo_upf
R 930 5 187 pseudo_types dion$sd pseudo_upf
R 931 5 188 pseudo_types dion$p pseudo_upf
R 932 5 189 pseudo_types dion$o pseudo_upf
R 934 5 191 pseudo_types q_with_l pseudo_upf
R 935 5 192 pseudo_types nqf pseudo_upf
R 936 5 193 pseudo_types nqlc pseudo_upf
R 937 5 194 pseudo_types qqq_eps pseudo_upf
R 938 5 195 pseudo_types rinner pseudo_upf
R 940 5 197 pseudo_types rinner$sd pseudo_upf
R 941 5 198 pseudo_types rinner$p pseudo_upf
R 942 5 199 pseudo_types rinner$o pseudo_upf
R 944 5 201 pseudo_types qqq pseudo_upf
R 947 5 204 pseudo_types qqq$sd pseudo_upf
R 948 5 205 pseudo_types qqq$p pseudo_upf
R 949 5 206 pseudo_types qqq$o pseudo_upf
R 951 5 208 pseudo_types qfunc pseudo_upf
R 954 5 211 pseudo_types qfunc$sd pseudo_upf
R 955 5 212 pseudo_types qfunc$p pseudo_upf
R 956 5 213 pseudo_types qfunc$o pseudo_upf
R 958 5 215 pseudo_types qfuncl pseudo_upf
R 962 5 219 pseudo_types qfuncl$sd pseudo_upf
R 963 5 220 pseudo_types qfuncl$p pseudo_upf
R 964 5 221 pseudo_types qfuncl$o pseudo_upf
R 966 5 223 pseudo_types qfcoef pseudo_upf
R 971 5 228 pseudo_types qfcoef$sd pseudo_upf
R 972 5 229 pseudo_types qfcoef$p pseudo_upf
R 973 5 230 pseudo_types qfcoef$o pseudo_upf
R 975 5 232 pseudo_types has_wfc pseudo_upf
R 976 5 233 pseudo_types aewfc pseudo_upf
R 979 5 236 pseudo_types aewfc$sd pseudo_upf
R 980 5 237 pseudo_types aewfc$p pseudo_upf
R 981 5 238 pseudo_types aewfc$o pseudo_upf
R 983 5 240 pseudo_types pswfc pseudo_upf
R 986 5 243 pseudo_types pswfc$sd pseudo_upf
R 987 5 244 pseudo_types pswfc$p pseudo_upf
R 988 5 245 pseudo_types pswfc$o pseudo_upf
R 990 5 247 pseudo_types has_so pseudo_upf
R 991 5 248 pseudo_types nn pseudo_upf
R 993 5 250 pseudo_types nn$sd pseudo_upf
R 994 5 251 pseudo_types nn$p pseudo_upf
R 995 5 252 pseudo_types nn$o pseudo_upf
R 997 5 254 pseudo_types rcut pseudo_upf
R 999 5 256 pseudo_types rcut$sd pseudo_upf
R 1000 5 257 pseudo_types rcut$p pseudo_upf
R 1001 5 258 pseudo_types rcut$o pseudo_upf
R 1003 5 260 pseudo_types rcutus pseudo_upf
R 1005 5 262 pseudo_types rcutus$sd pseudo_upf
R 1006 5 263 pseudo_types rcutus$p pseudo_upf
R 1007 5 264 pseudo_types rcutus$o pseudo_upf
R 1009 5 266 pseudo_types jchi pseudo_upf
R 1011 5 268 pseudo_types jchi$sd pseudo_upf
R 1012 5 269 pseudo_types jchi$p pseudo_upf
R 1013 5 270 pseudo_types jchi$o pseudo_upf
R 1015 5 272 pseudo_types jjj pseudo_upf
R 1017 5 274 pseudo_types jjj$sd pseudo_upf
R 1018 5 275 pseudo_types jjj$p pseudo_upf
R 1019 5 276 pseudo_types jjj$o pseudo_upf
R 1021 5 278 pseudo_types paw_data_format pseudo_upf
R 1022 5 279 pseudo_types tpawp pseudo_upf
R 1023 5 280 pseudo_types paw pseudo_upf
R 1024 5 281 pseudo_types grid pseudo_upf
R 1026 5 283 pseudo_types grid$p pseudo_upf
R 1028 5 285 pseudo_types has_gipaw pseudo_upf
R 1029 5 286 pseudo_types gipaw_data_format pseudo_upf
R 1030 5 287 pseudo_types gipaw_ncore_orbitals pseudo_upf
R 1031 5 288 pseudo_types gipaw_core_orbital_n pseudo_upf
R 1033 5 290 pseudo_types gipaw_core_orbital_n$sd pseudo_upf
R 1034 5 291 pseudo_types gipaw_core_orbital_n$p pseudo_upf
R 1035 5 292 pseudo_types gipaw_core_orbital_n$o pseudo_upf
R 1037 5 294 pseudo_types gipaw_core_orbital_l pseudo_upf
R 1039 5 296 pseudo_types gipaw_core_orbital_l$sd pseudo_upf
R 1040 5 297 pseudo_types gipaw_core_orbital_l$p pseudo_upf
R 1041 5 298 pseudo_types gipaw_core_orbital_l$o pseudo_upf
R 1043 5 300 pseudo_types gipaw_core_orbital_el pseudo_upf
R 1045 5 302 pseudo_types gipaw_core_orbital_el$sd pseudo_upf
R 1046 5 303 pseudo_types gipaw_core_orbital_el$p pseudo_upf
R 1047 5 304 pseudo_types gipaw_core_orbital_el$o pseudo_upf
R 1049 5 306 pseudo_types gipaw_core_orbital pseudo_upf
R 1052 5 309 pseudo_types gipaw_core_orbital$sd pseudo_upf
R 1053 5 310 pseudo_types gipaw_core_orbital$p pseudo_upf
R 1054 5 311 pseudo_types gipaw_core_orbital$o pseudo_upf
R 1056 5 313 pseudo_types gipaw_vlocal_ae pseudo_upf
R 1058 5 315 pseudo_types gipaw_vlocal_ae$sd pseudo_upf
R 1059 5 316 pseudo_types gipaw_vlocal_ae$p pseudo_upf
R 1060 5 317 pseudo_types gipaw_vlocal_ae$o pseudo_upf
R 1062 5 319 pseudo_types gipaw_vlocal_ps pseudo_upf
R 1064 5 321 pseudo_types gipaw_vlocal_ps$sd pseudo_upf
R 1065 5 322 pseudo_types gipaw_vlocal_ps$p pseudo_upf
R 1066 5 323 pseudo_types gipaw_vlocal_ps$o pseudo_upf
R 1068 5 325 pseudo_types gipaw_wfs_nchannels pseudo_upf
R 1069 5 326 pseudo_types gipaw_wfs_el pseudo_upf
R 1071 5 328 pseudo_types gipaw_wfs_el$sd pseudo_upf
R 1072 5 329 pseudo_types gipaw_wfs_el$p pseudo_upf
R 1073 5 330 pseudo_types gipaw_wfs_el$o pseudo_upf
R 1075 5 332 pseudo_types gipaw_wfs_ll pseudo_upf
R 1077 5 334 pseudo_types gipaw_wfs_ll$sd pseudo_upf
R 1078 5 335 pseudo_types gipaw_wfs_ll$p pseudo_upf
R 1079 5 336 pseudo_types gipaw_wfs_ll$o pseudo_upf
R 1081 5 338 pseudo_types gipaw_wfs_ae pseudo_upf
R 1084 5 341 pseudo_types gipaw_wfs_ae$sd pseudo_upf
R 1085 5 342 pseudo_types gipaw_wfs_ae$p pseudo_upf
R 1086 5 343 pseudo_types gipaw_wfs_ae$o pseudo_upf
R 1088 5 345 pseudo_types gipaw_wfs_rcut pseudo_upf
R 1090 5 347 pseudo_types gipaw_wfs_rcut$sd pseudo_upf
R 1091 5 348 pseudo_types gipaw_wfs_rcut$p pseudo_upf
R 1092 5 349 pseudo_types gipaw_wfs_rcut$o pseudo_upf
R 1094 5 351 pseudo_types gipaw_wfs_rcutus pseudo_upf
R 1096 5 353 pseudo_types gipaw_wfs_rcutus$sd pseudo_upf
R 1097 5 354 pseudo_types gipaw_wfs_rcutus$p pseudo_upf
R 1098 5 355 pseudo_types gipaw_wfs_rcutus$o pseudo_upf
R 1100 5 357 pseudo_types gipaw_wfs_ps pseudo_upf
R 1103 5 360 pseudo_types gipaw_wfs_ps$sd pseudo_upf
R 1104 5 361 pseudo_types gipaw_wfs_ps$p pseudo_upf
R 1105 5 362 pseudo_types gipaw_wfs_ps$o pseudo_upf
S 1122 23 5 0 0 0 1128 582 5367 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_upf_v1
S 1123 1 3 1 0 6 1 1122 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1124 1 3 3 0 459 1 1122 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1125 1 3 3 0 56 1 1122 5298 14 3008 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 grid
S 1126 1 3 2 0 6 1 1122 9061 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ierr
S 1127 1 3 1 0 16 1 1122 9066 80000014 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 header_only
S 1128 14 5 0 0 0 1 1122 5367 0 400000 A 0 0 0 0 0 0 0 51 5 0 0 0 0 0 0 0 0 0 0 0 0 27 0 582 0 0 0 0 read_upf_v1
F 1128 5 1123 1124 1125 1126 1127
S 1129 23 5 0 0 0 1133 582 5379 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 scan_begin
S 1130 1 3 0 0 6 1 1129 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1131 1 3 0 0 28 1 1129 9078 14 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 string
S 1132 1 3 0 0 16 1 1129 9085 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rew
S 1133 14 5 0 0 0 1 1129 5379 0 400000 A 0 0 0 0 0 0 0 57 3 0 0 0 0 0 0 0 0 0 0 0 0 165 0 582 0 0 0 0 scan_begin
F 1133 3 1130 1131 1132
S 1134 23 5 0 0 0 1137 582 5390 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 scan_end
S 1135 1 3 0 0 6 1 1134 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1136 1 3 0 0 28 1 1134 9078 14 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 string
S 1137 14 5 0 0 0 1 1134 5390 0 400000 A 0 0 0 0 0 0 0 61 2 0 0 0 0 0 0 0 0 0 0 0 0 191 0 582 0 0 0 0 scan_end
F 1137 2 1135 1136
S 1138 23 5 0 0 0 1141 582 9089 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_header
S 1139 1 3 3 0 178 1 1138 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1140 1 3 0 0 6 1 1138 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1141 14 5 0 0 0 1 1138 9089 10 400000 A 0 0 0 0 0 0 0 64 2 0 0 0 0 0 0 0 0 0 0 0 0 211 0 582 0 0 0 0 read_pseudo_header
F 1141 2 1139 1140
S 1142 23 5 0 0 0 1145 582 9108 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_mesh
S 1143 1 3 3 0 178 1 1142 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1144 1 3 0 0 6 1 1142 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1145 14 5 0 0 0 1 1142 9108 10 400000 A 0 0 0 0 0 0 0 67 2 0 0 0 0 0 0 0 0 0 0 0 0 283 0 582 0 0 0 0 read_pseudo_mesh
F 1145 2 1143 1144
S 1146 23 5 0 0 0 1149 582 9125 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_nlcc
S 1147 1 3 3 0 178 1 1146 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1148 1 3 0 0 6 1 1146 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1149 14 5 0 0 0 1 1146 9125 10 400000 A 0 0 0 0 0 0 0 70 2 0 0 0 0 0 0 0 0 0 0 0 0 322 0 582 0 0 0 0 read_pseudo_nlcc
F 1149 2 1147 1148
S 1150 23 5 0 0 0 1153 582 9142 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_local
S 1151 1 3 3 0 178 1 1150 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1152 1 3 0 0 6 1 1150 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1153 14 5 0 0 0 1 1150 9142 10 400000 A 0 0 0 0 0 0 0 73 2 0 0 0 0 0 0 0 0 0 0 0 0 347 0 582 0 0 0 0 read_pseudo_local
F 1153 2 1151 1152
S 1154 23 5 0 0 0 1157 582 9160 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_nl
S 1155 1 3 3 0 178 1 1154 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1156 1 3 0 0 6 1 1154 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1157 14 5 0 0 0 1 1154 9160 10 400000 A 0 0 0 0 0 0 0 76 2 0 0 0 0 0 0 0 0 0 0 0 0 373 0 582 0 0 0 0 read_pseudo_nl
F 1157 2 1155 1156
S 1158 23 5 0 0 0 1161 582 9175 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_pswfc
S 1159 1 3 3 0 178 1 1158 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1160 1 3 0 0 6 1 1158 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1161 14 5 0 0 0 1 1158 9175 10 400000 A 0 0 0 0 0 0 0 79 2 0 0 0 0 0 0 0 0 0 0 0 0 537 0 582 0 0 0 0 read_pseudo_pswfc
F 1161 2 1159 1160
S 1162 23 5 0 0 0 1165 582 9193 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_rhoatom
S 1163 1 3 3 0 178 1 1162 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1164 1 3 0 0 6 1 1162 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1165 14 5 0 0 0 1 1162 9193 10 400000 A 0 0 0 0 0 0 0 82 2 0 0 0 0 0 0 0 0 0 0 0 0 564 0 582 0 0 0 0 read_pseudo_rhoatom
F 1165 2 1163 1164
S 1166 23 5 0 0 0 1169 582 9213 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_addinfo
S 1167 1 3 3 0 178 1 1166 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1168 1 3 0 0 6 1 1166 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1169 14 5 0 0 0 1 1166 9213 10 400000 A 0 0 0 0 0 0 0 85 2 0 0 0 0 0 0 0 0 0 0 0 0 587 0 582 0 0 0 0 read_pseudo_addinfo
F 1169 2 1167 1168
S 1170 23 5 0 0 0 1173 582 9233 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_gipaw
S 1171 1 3 3 0 178 1 1170 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1172 1 3 0 0 6 1 1170 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1173 14 5 0 0 0 1 1170 9233 10 400000 A 0 0 0 0 0 0 0 88 2 0 0 0 0 0 0 0 0 0 0 0 0 642 0 582 0 0 0 0 read_pseudo_gipaw
F 1173 2 1171 1172
S 1174 23 5 0 0 0 1177 582 9251 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_gipaw_core_orbitals
S 1175 1 3 3 0 178 1 1174 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1176 1 3 0 0 6 1 1174 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1177 14 5 0 0 0 1 1174 9251 10 400000 A 0 0 0 0 0 0 0 91 2 0 0 0 0 0 0 0 0 0 0 0 0 673 0 582 0 0 0 0 read_pseudo_gipaw_core_orbitals
F 1177 2 1175 1176
S 1178 23 5 0 0 0 1181 582 9283 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_gipaw_local
S 1179 1 3 3 0 178 1 1178 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1180 1 3 0 0 6 1 1178 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1181 14 5 0 0 0 1 1178 9283 10 400000 A 0 0 0 0 0 0 0 94 2 0 0 0 0 0 0 0 0 0 0 0 0 714 0 582 0 0 0 0 read_pseudo_gipaw_local
F 1181 2 1179 1180
S 1182 23 5 0 0 0 1185 582 9307 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_gipaw_orbitals
S 1183 1 3 3 0 178 1 1182 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1184 1 3 0 0 6 1 1182 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1185 14 5 0 0 0 1 1182 9307 10 400000 A 0 0 0 0 0 0 0 97 2 0 0 0 0 0 0 0 0 0 0 0 0 755 0 582 0 0 0 0 read_pseudo_gipaw_orbitals
F 1185 2 1183 1184
S 1186 23 5 0 0 0 1189 582 9334 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_pseudo_ppinfo
S 1187 1 3 3 0 178 1 1186 9029 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 upf
S 1188 1 3 0 0 6 1 1186 9055 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 iunps
S 1189 14 5 0 0 0 1 1186 9334 10 400000 A 0 0 0 0 0 0 0 100 2 0 0 0 0 0 0 0 0 0 0 0 0 804 0 582 0 0 0 0 read_pseudo_ppinfo
F 1189 2 1187 1188
A 593 2 0 0 511 184 734 0 0 0 593 0 0 0 0 0 0 0 0 0
Z
T 794 178 0 3 0 0
A 795 184 0 0 1 593 1
A 796 184 0 0 1 593 1
A 797 184 0 0 1 593 1
A 798 184 0 0 1 593 1
A 799 184 0 0 1 593 1
A 800 184 0 0 1 593 1
A 801 184 0 0 1 593 0
Z
