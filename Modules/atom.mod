V26 atom
8 atom.F90 S582 0
04/24/2012  15:51:12
use radial_grids private
use kinds private
use radial_grids private
use kinds private
enduse
D 56 24 606 656 605 7
D 128 21 56 1 95 94 0 1 0 0 1
 89 92 93 89 92 90
D 131 21 6 1 0 19 0 0 0 0 0
 0 19 0 3 19 0
D 134 21 6 1 104 103 0 1 0 0 1
 98 101 102 98 101 99
D 137 21 6 1 0 19 0 0 0 0 0
 0 19 0 3 19 0
S 582 24 0 0 0 9 1 0 4658 5 0 A 0 0 0 0 0 0 0 0 0 0 0 0 28 0 0 0 0 0 0 0 0 12 0 0 0 0 0 0 atom
S 584 23 0 0 0 9 589 582 4669 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 9 605 582 4685 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 radial_grid_type
S 587 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 8 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 589 16 1 kinds dp
S 597 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 18 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 598 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 13 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 599 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 600 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 601 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 605 25 3 radial_grids radial_grid_type
R 606 5 4 radial_grids mesh radial_grid_type
R 607 5 5 radial_grids r radial_grid_type
R 609 5 7 radial_grids r$sd radial_grid_type
R 610 5 8 radial_grids r$p radial_grid_type
R 611 5 9 radial_grids r$o radial_grid_type
R 613 5 11 radial_grids r2 radial_grid_type
R 615 5 13 radial_grids r2$sd radial_grid_type
R 616 5 14 radial_grids r2$p radial_grid_type
R 617 5 15 radial_grids r2$o radial_grid_type
R 619 5 17 radial_grids rab radial_grid_type
R 621 5 19 radial_grids rab$sd radial_grid_type
R 622 5 20 radial_grids rab$p radial_grid_type
R 623 5 21 radial_grids rab$o radial_grid_type
R 625 5 23 radial_grids sqr radial_grid_type
R 627 5 25 radial_grids sqr$sd radial_grid_type
R 628 5 26 radial_grids sqr$p radial_grid_type
R 629 5 27 radial_grids sqr$o radial_grid_type
R 631 5 29 radial_grids rm1 radial_grid_type
R 633 5 31 radial_grids rm1$sd radial_grid_type
R 634 5 32 radial_grids rm1$p radial_grid_type
R 635 5 33 radial_grids rm1$o radial_grid_type
R 637 5 35 radial_grids rm2 radial_grid_type
R 639 5 37 radial_grids rm2$sd radial_grid_type
R 640 5 38 radial_grids rm2$p radial_grid_type
R 641 5 39 radial_grids rm2$o radial_grid_type
R 643 5 41 radial_grids rm3 radial_grid_type
R 645 5 43 radial_grids rm3$sd radial_grid_type
R 646 5 44 radial_grids rm3$p radial_grid_type
R 647 5 45 radial_grids rm3$o radial_grid_type
R 649 5 47 radial_grids xmin radial_grid_type
R 650 5 48 radial_grids rmax radial_grid_type
R 651 5 49 radial_grids zmesh radial_grid_type
R 652 5 50 radial_grids dx radial_grid_type
S 722 7 6 0 0 128 1 582 5353 10a00004 59 A 0 0 0 0 0 0 725 0 0 0 727 0 0 0 0 0 0 0 0 724 0 0 726 582 0 0 0 0 rgrid
S 723 6 4 0 0 6 729 582 5289 40800006 0 A 0 0 0 0 0 0 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 z_b_0_1
S 724 8 4 0 0 131 731 582 5359 40822064 1020 A 0 0 0 0 0 0 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 rgrid$sd
S 725 6 4 0 0 7 726 582 5368 40802061 1020 A 0 0 0 0 0 72 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 rgrid$p
S 726 6 4 0 0 7 724 582 5376 40802060 1020 A 0 0 0 0 0 80 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 rgrid$o
S 727 22 1 0 0 9 1 582 5384 40000000 1000 A 0 0 0 0 0 0 0 722 0 0 0 0 724 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 rgrid$arrdsc
S 728 7 6 0 0 134 1 582 5397 10a00004 51 A 0 0 0 0 0 0 731 0 0 0 733 0 0 0 0 0 0 0 0 730 0 0 732 582 0 0 0 0 msh
S 729 6 4 0 0 6 1 582 5401 40800006 0 A 0 0 0 0 0 4 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 z_b_1_1
S 730 8 4 0 0 137 723 582 5409 40822064 1020 A 0 0 0 0 0 88 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 msh$sd
S 731 6 4 0 0 7 732 582 5416 40802061 1020 A 0 0 0 0 0 160 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 msh$p
S 732 6 4 0 0 7 730 582 5422 40802060 1020 A 0 0 0 0 0 168 0 0 0 0 0 0 734 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 msh$o
S 733 22 1 0 0 6 1 582 5428 40000000 1000 A 0 0 0 0 0 0 0 728 0 0 0 0 730 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 msh$arrdsc
S 734 11 0 0 0 9 666 582 5439 40800000 801000 A 0 0 0 0 0 184 0 0 725 729 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 _atom$0
A 13 2 0 0 0 6 587 0 0 0 13 0 0 0 0 0 0 0 0 0
A 19 2 0 0 0 6 597 0 0 0 19 0 0 0 0 0 0 0 0 0
A 22 2 0 0 0 6 598 0 0 0 22 0 0 0 0 0 0 0 0 0
A 24 2 0 0 0 6 599 0 0 0 24 0 0 0 0 0 0 0 0 0
A 28 2 0 0 0 6 600 0 0 0 28 0 0 0 0 0 0 0 0 0
A 30 2 0 0 0 6 601 0 0 0 30 0 0 0 0 0 0 0 0 0
A 88 1 0 1 0 131 724 0 0 0 0 0 0 0 0 0 0 0 0 0
A 89 10 0 0 0 6 88 4 0 0 0 0 0 0 0 0 0 0 0 0
X 1 22
A 90 10 0 0 89 6 88 7 0 0 0 0 0 0 0 0 0 0 0 0
X 1 24
A 91 4 0 0 0 6 90 0 3 0 0 0 0 2 0 0 0 0 0 0
A 92 4 0 0 0 6 89 0 91 0 0 0 0 1 0 0 0 0 0 0
A 93 10 0 0 90 6 88 10 0 0 0 0 0 0 0 0 0 0 0 0
X 1 28
A 94 10 0 0 93 6 88 13 0 0 0 0 0 0 0 0 0 0 0 0
X 1 30
A 95 10 0 0 94 6 88 1 0 0 0 0 0 0 0 0 0 0 0 0
X 1 13
A 97 1 0 1 53 137 730 0 0 0 0 0 0 0 0 0 0 0 0 0
A 98 10 0 0 0 6 97 4 0 0 0 0 0 0 0 0 0 0 0 0
X 1 22
A 99 10 0 0 98 6 97 7 0 0 0 0 0 0 0 0 0 0 0 0
X 1 24
A 100 4 0 0 15 6 99 0 3 0 0 0 0 2 0 0 0 0 0 0
A 101 4 0 0 0 6 98 0 100 0 0 0 0 1 0 0 0 0 0 0
A 102 10 0 0 99 6 97 10 0 0 0 0 0 0 0 0 0 0 0 0
X 1 28
A 103 10 0 0 102 6 97 13 0 0 0 0 0 0 0 0 0 0 0 0
X 1 30
A 104 10 0 0 103 6 97 1 0 0 0 0 0 0 0 0 0 0 0 0
X 1 13
Z
Z
