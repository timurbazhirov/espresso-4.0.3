V26 grid_subroutines
11 griddim.F90 S582 0
04/24/2012  15:51:15
use fft_types private
use kinds private
use fft_types private
use kinds private
enduse
D 56 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 59 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 62 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 227 24 642 1584 641 7
D 233 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 236 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 239 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
D 242 21 9 1 3 17 0 0 0 0 0
 0 17 3 3 17 17
S 582 24 0 0 0 9 1 0 4658 10005 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 100 0 0 0 0 0 0 grid_subroutines
S 584 23 0 0 0 9 587 582 4681 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
R 587 16 1 kinds dp
S 594 23 5 0 0 0 603 582 4715 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 realspace_grids_init
S 595 1 3 1 0 9 1 594 4736 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 alat
S 596 7 3 1 0 56 1 594 4741 800004 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a1
S 597 7 3 1 0 59 1 594 4744 800004 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a2
S 598 7 3 1 0 62 1 594 4747 800004 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a3
S 599 1 3 1 0 9 1 594 4750 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 gcutd
S 600 1 3 1 0 9 1 594 4756 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 gcuts
S 601 1 3 2 0 6 1 594 4762 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ng
S 602 1 3 2 0 6 1 594 4765 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ngs
S 603 14 5 0 0 0 1 594 4715 0 400000 A 0 0 0 0 0 0 0 4 8 0 0 0 0 0 0 0 0 0 0 0 0 114 0 582 0 0 0 0 realspace_grids_init
F 603 8 595 596 597 598 599 600 601 602
S 604 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 641 25 2 fft_types fft_dlay_descriptor
R 642 5 3 fft_types nst fft_dlay_descriptor
R 643 5 4 fft_types nsp fft_dlay_descriptor
R 645 5 6 fft_types nsp$sd fft_dlay_descriptor
R 646 5 7 fft_types nsp$p fft_dlay_descriptor
R 647 5 8 fft_types nsp$o fft_dlay_descriptor
R 649 5 10 fft_types nsw fft_dlay_descriptor
R 651 5 12 fft_types nsw$sd fft_dlay_descriptor
R 652 5 13 fft_types nsw$p fft_dlay_descriptor
R 653 5 14 fft_types nsw$o fft_dlay_descriptor
R 655 5 16 fft_types nr1 fft_dlay_descriptor
R 656 5 17 fft_types nr2 fft_dlay_descriptor
R 657 5 18 fft_types nr3 fft_dlay_descriptor
R 658 5 19 fft_types nr1x fft_dlay_descriptor
R 659 5 20 fft_types nr2x fft_dlay_descriptor
R 660 5 21 fft_types nr3x fft_dlay_descriptor
R 661 5 22 fft_types npl fft_dlay_descriptor
R 662 5 23 fft_types nnp fft_dlay_descriptor
R 663 5 24 fft_types nnr fft_dlay_descriptor
R 664 5 25 fft_types ngt fft_dlay_descriptor
R 665 5 26 fft_types ngl fft_dlay_descriptor
R 667 5 28 fft_types ngl$sd fft_dlay_descriptor
R 668 5 29 fft_types ngl$p fft_dlay_descriptor
R 669 5 30 fft_types ngl$o fft_dlay_descriptor
R 671 5 32 fft_types nwl fft_dlay_descriptor
R 673 5 34 fft_types nwl$sd fft_dlay_descriptor
R 674 5 35 fft_types nwl$p fft_dlay_descriptor
R 675 5 36 fft_types nwl$o fft_dlay_descriptor
R 677 5 38 fft_types npp fft_dlay_descriptor
R 679 5 40 fft_types npp$sd fft_dlay_descriptor
R 680 5 41 fft_types npp$p fft_dlay_descriptor
R 681 5 42 fft_types npp$o fft_dlay_descriptor
R 683 5 44 fft_types ipp fft_dlay_descriptor
R 685 5 46 fft_types ipp$sd fft_dlay_descriptor
R 686 5 47 fft_types ipp$p fft_dlay_descriptor
R 687 5 48 fft_types ipp$o fft_dlay_descriptor
R 689 5 50 fft_types iss fft_dlay_descriptor
R 691 5 52 fft_types iss$sd fft_dlay_descriptor
R 692 5 53 fft_types iss$p fft_dlay_descriptor
R 693 5 54 fft_types iss$o fft_dlay_descriptor
R 695 5 56 fft_types isind fft_dlay_descriptor
R 697 5 58 fft_types isind$sd fft_dlay_descriptor
R 698 5 59 fft_types isind$p fft_dlay_descriptor
R 699 5 60 fft_types isind$o fft_dlay_descriptor
R 701 5 62 fft_types ismap fft_dlay_descriptor
R 703 5 64 fft_types ismap$sd fft_dlay_descriptor
R 704 5 65 fft_types ismap$p fft_dlay_descriptor
R 705 5 66 fft_types ismap$o fft_dlay_descriptor
R 707 5 68 fft_types iplp fft_dlay_descriptor
R 709 5 70 fft_types iplp$sd fft_dlay_descriptor
R 710 5 71 fft_types iplp$p fft_dlay_descriptor
R 711 5 72 fft_types iplp$o fft_dlay_descriptor
R 713 5 74 fft_types iplw fft_dlay_descriptor
R 715 5 76 fft_types iplw$sd fft_dlay_descriptor
R 716 5 77 fft_types iplw$p fft_dlay_descriptor
R 717 5 78 fft_types iplw$o fft_dlay_descriptor
R 719 5 80 fft_types id fft_dlay_descriptor
R 720 5 81 fft_types tptr fft_dlay_descriptor
R 721 5 82 fft_types irb fft_dlay_descriptor
R 724 5 85 fft_types irb$sd fft_dlay_descriptor
R 725 5 86 fft_types irb$p fft_dlay_descriptor
R 726 5 87 fft_types irb$o fft_dlay_descriptor
R 728 5 89 fft_types imin3 fft_dlay_descriptor
R 730 5 91 fft_types imin3$sd fft_dlay_descriptor
R 731 5 92 fft_types imin3$p fft_dlay_descriptor
R 732 5 93 fft_types imin3$o fft_dlay_descriptor
R 734 5 95 fft_types imax3 fft_dlay_descriptor
R 736 5 97 fft_types imax3$sd fft_dlay_descriptor
R 737 5 98 fft_types imax3$p fft_dlay_descriptor
R 738 5 99 fft_types imax3$o fft_dlay_descriptor
R 740 5 101 fft_types np3 fft_dlay_descriptor
R 742 5 103 fft_types np3$sd fft_dlay_descriptor
R 743 5 104 fft_types np3$p fft_dlay_descriptor
R 744 5 105 fft_types np3$o fft_dlay_descriptor
R 746 5 107 fft_types have_task_groups fft_dlay_descriptor
R 747 5 108 fft_types nnrx fft_dlay_descriptor
R 748 5 109 fft_types tg_nsw fft_dlay_descriptor
R 750 5 111 fft_types tg_nsw$sd fft_dlay_descriptor
R 751 5 112 fft_types tg_nsw$p fft_dlay_descriptor
R 752 5 113 fft_types tg_nsw$o fft_dlay_descriptor
R 754 5 115 fft_types tg_npp fft_dlay_descriptor
R 756 5 117 fft_types tg_npp$sd fft_dlay_descriptor
R 757 5 118 fft_types tg_npp$p fft_dlay_descriptor
R 758 5 119 fft_types tg_npp$o fft_dlay_descriptor
S 908 23 5 0 0 0 911 582 6581 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 realspace_grids_para
S 909 1 3 1 0 227 1 908 6602 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 dfftp
S 910 1 3 1 0 227 1 908 6608 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 dffts
S 911 14 5 0 0 0 1 908 6581 0 400000 A 0 0 0 0 0 0 0 84 2 0 0 0 0 0 0 0 0 0 0 0 0 199 0 582 0 0 0 0 realspace_grids_para
F 911 2 909 910
S 912 23 5 0 0 0 923 582 6614 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ngnr_set
S 913 1 3 1 0 9 1 912 4736 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 alat
S 914 7 3 1 0 233 1 912 4741 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a1
S 915 7 3 1 0 236 1 912 4744 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a2
S 916 7 3 1 0 239 1 912 4747 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 a3
S 917 1 3 1 0 9 1 912 6623 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 gcut
S 918 7 3 1 0 242 1 912 6628 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 qk
S 919 1 3 2 0 6 1 912 4762 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ng
S 920 1 3 2 0 6 1 912 5066 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nr1
S 921 1 3 2 0 6 1 912 5070 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nr2
S 922 1 3 2 0 6 1 912 5074 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nr3
S 923 14 5 0 0 0 1 912 6614 0 400000 A 0 0 0 0 0 0 0 87 10 0 0 0 0 0 0 0 0 0 0 0 0 288 0 582 0 0 0 0 ngnr_set
F 923 10 913 914 915 916 917 918 919 920 921 922
A 17 2 0 0 0 6 604 0 0 0 17 0 0 0 0 0 0 0 0 0
Z
Z
