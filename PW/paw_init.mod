V26 paw_init
12 paw_init.F90 S582 0
04/24/2012  15:55:15
use paw_variables private
use kinds private
use paw_variables private
use kinds private
enduse
D 122 24 636 640 635 7
S 582 24 0 0 0 9 1 0 4658 10005 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 26 0 0 0 0 0 0 paw_init
S 584 23 0 0 0 9 587 582 4673 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
R 587 16 1 kinds dp
S 594 27 0 0 0 9 605 582 4707 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 paw_atomic_becsum
S 595 27 0 0 0 9 607 582 4725 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 paw_init_onecenter
S 596 27 0 0 0 9 603 582 4744 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 paw_post_init
S 597 27 0 0 0 9 599 582 4758 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 allocate_paw_internals
S 598 27 0 0 0 9 601 582 4781 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 deallocate_paw_internals
S 599 23 5 0 0 0 600 582 4758 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 allocate_paw_internals
S 600 14 5 0 0 0 1 599 4758 0 400000 A 0 0 0 0 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 0 0 43 0 582 0 0 0 0 allocate_paw_internals
F 600 0
S 601 23 5 0 0 0 602 582 4781 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 deallocate_paw_internals
S 602 14 5 0 0 0 1 601 4781 0 400000 A 0 0 0 0 0 0 0 5 0 0 0 0 0 0 0 0 0 0 0 0 0 57 0 582 0 0 0 0 deallocate_paw_internals
F 602 0
S 603 23 5 0 0 0 604 582 4744 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 paw_post_init
S 604 14 5 0 0 0 1 603 4744 0 400000 A 0 0 0 0 0 0 0 6 0 0 0 0 0 0 0 0 0 0 0 0 0 86 0 582 0 0 0 0 paw_post_init
F 604 0
S 605 23 5 0 0 0 606 582 4707 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 paw_atomic_becsum
S 606 14 5 0 0 0 1 605 4707 0 400000 A 0 0 0 0 0 0 0 7 0 0 0 0 0 0 0 0 0 0 0 0 0 141 0 582 0 0 0 0 paw_atomic_becsum
F 606 0
S 607 23 5 0 0 0 608 582 4725 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 paw_init_onecenter
S 608 14 5 0 0 0 1 607 4725 0 400000 A 0 0 0 0 0 0 0 8 0 0 0 0 0 0 0 0 0 0 0 0 0 216 0 582 0 0 0 0 paw_init_onecenter
F 608 0
R 635 25 10 paw_variables paw_radial_integrator
R 636 5 11 paw_variables lmax paw_radial_integrator
R 637 5 12 paw_variables ladd paw_radial_integrator
R 638 5 13 paw_variables lm_max paw_radial_integrator
R 639 5 14 paw_variables nx paw_radial_integrator
R 640 5 15 paw_variables ww paw_radial_integrator
R 642 5 17 paw_variables ww$sd paw_radial_integrator
R 643 5 18 paw_variables ww$p paw_radial_integrator
R 644 5 19 paw_variables ww$o paw_radial_integrator
R 646 5 21 paw_variables ylm paw_radial_integrator
R 649 5 24 paw_variables ylm$sd paw_radial_integrator
R 650 5 25 paw_variables ylm$p paw_radial_integrator
R 651 5 26 paw_variables ylm$o paw_radial_integrator
R 653 5 28 paw_variables wwylm paw_radial_integrator
R 656 5 31 paw_variables wwylm$sd paw_radial_integrator
R 657 5 32 paw_variables wwylm$p paw_radial_integrator
R 658 5 33 paw_variables wwylm$o paw_radial_integrator
R 660 5 35 paw_variables dylmt paw_radial_integrator
R 663 5 38 paw_variables dylmt$sd paw_radial_integrator
R 664 5 39 paw_variables dylmt$p paw_radial_integrator
R 665 5 40 paw_variables dylmt$o paw_radial_integrator
R 667 5 42 paw_variables dylmp paw_radial_integrator
R 670 5 45 paw_variables dylmp$sd paw_radial_integrator
R 671 5 46 paw_variables dylmp$p paw_radial_integrator
R 672 5 47 paw_variables dylmp$o paw_radial_integrator
R 674 5 49 paw_variables cotg_th paw_radial_integrator
R 676 5 51 paw_variables cotg_th$sd paw_radial_integrator
R 677 5 52 paw_variables cotg_th$p paw_radial_integrator
R 678 5 53 paw_variables cotg_th$o paw_radial_integrator
S 705 23 5 0 0 0 709 582 5508 0 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 paw_rad_init
S 706 1 3 1 0 6 1 705 5314 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 l
S 707 1 3 1 0 6 1 705 5521 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ls
S 708 1 3 2 0 122 1 705 5256 4 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rad
S 709 14 5 0 0 0 1 705 5508 0 400000 A 0 0 0 0 0 0 0 9 3 0 0 0 0 0 0 0 0 0 0 0 0 314 0 582 0 0 0 0 paw_rad_init
F 709 3 706 707 708
Z
Z
