V26 io_rho_xml
14 io_rho_xml.F90 S582 0
04/24/2012  15:55:16
use pseudo_types private
use gvect private
use scf private
use xml_io_base private
use kinds private
use pseudo_types private
use gvect private
use scf private
use xml_io_base private
use kinds private
enduse
D 1240 24 4046 5360 4045 7
D 1246 18 2
D 1555 18 2
D 1557 24 4411 744 4410 7
D 1599 20 7
D 1601 20 7
D 1603 20 7
D 1605 20 7
D 1607 20 7
D 1609 20 7
D 1611 24 4459 520 4456 7
D 1641 20 7
D 1643 20 7
D 1645 20 7
D 1647 20 7
D 1697 24 4411 744 4410 7
D 1703 21 9 2 2805 2810 0 0 1 0 0
 0 2806 3 3 2807 2807
 0 2808 2807 3 2809 2809
D 1706 21 9 2 2811 2815 0 0 1 0 0
 0 2806 3 3 2812 2812
 0 2813 2812 3 2814 2814
S 582 24 0 0 0 6 1 0 4658 10015 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 27 0 0 0 0 0 0 io_rho_xml
S 584 23 0 0 0 9 591 582 4675 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 dp
S 586 23 0 0 0 9 2161 582 4690 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 create_directory
S 587 23 0 0 0 9 2491 582 4707 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 write_rho_xml
S 588 23 0 0 0 9 2514 582 4721 14 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 read_rho_xml
R 591 16 1 kinds dp
R 2161 14 49 xml_io_base create_directory
R 2491 14 379 xml_io_base write_rho_xml
R 2514 14 402 xml_io_base read_rho_xml
S 2600 19 0 0 0 9 1 582 14573 4000 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 149 2 0 0 0 0 0 582 0 0 0 0 write_rho
O 2600 2 2603 2602
S 2601 19 0 0 0 9 1 582 14583 4000 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 153 2 0 0 0 0 0 582 0 0 0 0 read_rho
O 2601 2 2605 2604
S 2602 27 0 0 0 9 4636 582 14592 10010 400000 A 0 0 0 0 0 0 164 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 write_rho_only
Q 2602 2600 0
S 2603 27 0 0 0 9 4626 582 14607 10010 400000 A 0 0 0 0 0 0 162 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 write_rho_general
Q 2603 2600 0
S 2604 27 0 0 0 9 4645 582 14625 10010 400000 A 0 0 0 0 0 0 165 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 read_rho_only
Q 2604 2601 0
S 2605 27 0 0 0 9 4631 582 14639 10010 400000 A 0 0 0 0 0 0 163 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 read_rho_general
Q 2605 2601 0
R 3677 6 14 gvect nrxx
S 3994 3 0 0 0 1555 1 1 0 0 0 A 0 0 0 0 0 0 0 0 20962 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 18 0
R 4045 25 51 pseudo_types pseudo_upf
R 4046 5 52 pseudo_types generated pseudo_upf
R 4047 5 53 pseudo_types author pseudo_upf
R 4048 5 54 pseudo_types date pseudo_upf
R 4049 5 55 pseudo_types comment pseudo_upf
R 4050 5 56 pseudo_types psd pseudo_upf
R 4051 5 57 pseudo_types typ pseudo_upf
R 4052 5 58 pseudo_types rel pseudo_upf
R 4053 5 59 pseudo_types tvanp pseudo_upf
R 4054 5 60 pseudo_types tcoulombp pseudo_upf
R 4055 5 61 pseudo_types nlcc pseudo_upf
R 4056 5 62 pseudo_types dft pseudo_upf
R 4057 5 63 pseudo_types zp pseudo_upf
R 4058 5 64 pseudo_types etotps pseudo_upf
R 4059 5 65 pseudo_types ecutwfc pseudo_upf
R 4060 5 66 pseudo_types ecutrho pseudo_upf
R 4061 5 67 pseudo_types nv pseudo_upf
R 4062 5 68 pseudo_types lmax pseudo_upf
R 4063 5 69 pseudo_types lmax_rho pseudo_upf
R 4064 5 70 pseudo_types nwfc pseudo_upf
R 4065 5 71 pseudo_types nbeta pseudo_upf
R 4066 5 72 pseudo_types kbeta pseudo_upf
R 4068 5 74 pseudo_types kbeta$sd pseudo_upf
R 4069 5 75 pseudo_types kbeta$p pseudo_upf
R 4070 5 76 pseudo_types kbeta$o pseudo_upf
R 4072 5 78 pseudo_types kkbeta pseudo_upf
R 4073 5 79 pseudo_types lll pseudo_upf
R 4075 5 81 pseudo_types lll$sd pseudo_upf
R 4076 5 82 pseudo_types lll$p pseudo_upf
R 4077 5 83 pseudo_types lll$o pseudo_upf
R 4079 5 85 pseudo_types beta pseudo_upf
R 4082 5 88 pseudo_types beta$sd pseudo_upf
R 4083 5 89 pseudo_types beta$p pseudo_upf
R 4084 5 90 pseudo_types beta$o pseudo_upf
R 4086 5 92 pseudo_types els pseudo_upf
R 4088 5 94 pseudo_types els$sd pseudo_upf
R 4089 5 95 pseudo_types els$p pseudo_upf
R 4090 5 96 pseudo_types els$o pseudo_upf
R 4092 5 98 pseudo_types els_beta pseudo_upf
R 4094 5 100 pseudo_types els_beta$sd pseudo_upf
R 4095 5 101 pseudo_types els_beta$p pseudo_upf
R 4096 5 102 pseudo_types els_beta$o pseudo_upf
R 4098 5 104 pseudo_types nchi pseudo_upf
R 4100 5 106 pseudo_types nchi$sd pseudo_upf
R 4101 5 107 pseudo_types nchi$p pseudo_upf
R 4102 5 108 pseudo_types nchi$o pseudo_upf
R 4104 5 110 pseudo_types lchi pseudo_upf
R 4106 5 112 pseudo_types lchi$sd pseudo_upf
R 4107 5 113 pseudo_types lchi$p pseudo_upf
R 4108 5 114 pseudo_types lchi$o pseudo_upf
R 4111 5 117 pseudo_types oc pseudo_upf
R 4112 5 118 pseudo_types oc$sd pseudo_upf
R 4113 5 119 pseudo_types oc$p pseudo_upf
R 4114 5 120 pseudo_types oc$o pseudo_upf
R 4116 5 122 pseudo_types epseu pseudo_upf
R 4118 5 124 pseudo_types epseu$sd pseudo_upf
R 4119 5 125 pseudo_types epseu$p pseudo_upf
R 4120 5 126 pseudo_types epseu$o pseudo_upf
R 4122 5 128 pseudo_types rcut_chi pseudo_upf
R 4124 5 130 pseudo_types rcut_chi$sd pseudo_upf
R 4125 5 131 pseudo_types rcut_chi$p pseudo_upf
R 4126 5 132 pseudo_types rcut_chi$o pseudo_upf
R 4128 5 134 pseudo_types rcutus_chi pseudo_upf
R 4130 5 136 pseudo_types rcutus_chi$sd pseudo_upf
R 4131 5 137 pseudo_types rcutus_chi$p pseudo_upf
R 4132 5 138 pseudo_types rcutus_chi$o pseudo_upf
R 4134 5 140 pseudo_types chi pseudo_upf
R 4137 5 143 pseudo_types chi$sd pseudo_upf
R 4138 5 144 pseudo_types chi$p pseudo_upf
R 4139 5 145 pseudo_types chi$o pseudo_upf
R 4141 5 147 pseudo_types rho_at pseudo_upf
R 4143 5 149 pseudo_types rho_at$sd pseudo_upf
R 4144 5 150 pseudo_types rho_at$p pseudo_upf
R 4145 5 151 pseudo_types rho_at$o pseudo_upf
R 4147 5 153 pseudo_types mesh pseudo_upf
R 4148 5 154 pseudo_types xmin pseudo_upf
R 4149 5 155 pseudo_types rmax pseudo_upf
R 4150 5 156 pseudo_types zmesh pseudo_upf
R 4151 5 157 pseudo_types dx pseudo_upf
R 4153 5 159 pseudo_types r pseudo_upf
R 4154 5 160 pseudo_types r$sd pseudo_upf
R 4155 5 161 pseudo_types r$p pseudo_upf
R 4156 5 162 pseudo_types r$o pseudo_upf
R 4159 5 165 pseudo_types rab pseudo_upf
R 4160 5 166 pseudo_types rab$sd pseudo_upf
R 4161 5 167 pseudo_types rab$p pseudo_upf
R 4162 5 168 pseudo_types rab$o pseudo_upf
R 4164 5 170 pseudo_types rho_atc pseudo_upf
R 4166 5 172 pseudo_types rho_atc$sd pseudo_upf
R 4167 5 173 pseudo_types rho_atc$p pseudo_upf
R 4168 5 174 pseudo_types rho_atc$o pseudo_upf
R 4170 5 176 pseudo_types lloc pseudo_upf
R 4171 5 177 pseudo_types rcloc pseudo_upf
R 4172 5 178 pseudo_types vloc pseudo_upf
R 4174 5 180 pseudo_types vloc$sd pseudo_upf
R 4175 5 181 pseudo_types vloc$p pseudo_upf
R 4176 5 182 pseudo_types vloc$o pseudo_upf
R 4178 5 184 pseudo_types dion pseudo_upf
R 4181 5 187 pseudo_types dion$sd pseudo_upf
R 4182 5 188 pseudo_types dion$p pseudo_upf
R 4183 5 189 pseudo_types dion$o pseudo_upf
R 4185 5 191 pseudo_types q_with_l pseudo_upf
R 4186 5 192 pseudo_types nqf pseudo_upf
R 4187 5 193 pseudo_types nqlc pseudo_upf
R 4188 5 194 pseudo_types qqq_eps pseudo_upf
R 4189 5 195 pseudo_types rinner pseudo_upf
R 4191 5 197 pseudo_types rinner$sd pseudo_upf
R 4192 5 198 pseudo_types rinner$p pseudo_upf
R 4193 5 199 pseudo_types rinner$o pseudo_upf
R 4195 5 201 pseudo_types qqq pseudo_upf
R 4198 5 204 pseudo_types qqq$sd pseudo_upf
R 4199 5 205 pseudo_types qqq$p pseudo_upf
R 4200 5 206 pseudo_types qqq$o pseudo_upf
R 4202 5 208 pseudo_types qfunc pseudo_upf
R 4205 5 211 pseudo_types qfunc$sd pseudo_upf
R 4206 5 212 pseudo_types qfunc$p pseudo_upf
R 4207 5 213 pseudo_types qfunc$o pseudo_upf
R 4209 5 215 pseudo_types qfuncl pseudo_upf
R 4213 5 219 pseudo_types qfuncl$sd pseudo_upf
R 4214 5 220 pseudo_types qfuncl$p pseudo_upf
R 4215 5 221 pseudo_types qfuncl$o pseudo_upf
R 4217 5 223 pseudo_types qfcoef pseudo_upf
R 4222 5 228 pseudo_types qfcoef$sd pseudo_upf
R 4223 5 229 pseudo_types qfcoef$p pseudo_upf
R 4224 5 230 pseudo_types qfcoef$o pseudo_upf
R 4226 5 232 pseudo_types has_wfc pseudo_upf
R 4227 5 233 pseudo_types aewfc pseudo_upf
R 4230 5 236 pseudo_types aewfc$sd pseudo_upf
R 4231 5 237 pseudo_types aewfc$p pseudo_upf
R 4232 5 238 pseudo_types aewfc$o pseudo_upf
R 4234 5 240 pseudo_types pswfc pseudo_upf
R 4237 5 243 pseudo_types pswfc$sd pseudo_upf
R 4238 5 244 pseudo_types pswfc$p pseudo_upf
R 4239 5 245 pseudo_types pswfc$o pseudo_upf
R 4241 5 247 pseudo_types has_so pseudo_upf
R 4242 5 248 pseudo_types nn pseudo_upf
R 4244 5 250 pseudo_types nn$sd pseudo_upf
R 4245 5 251 pseudo_types nn$p pseudo_upf
R 4246 5 252 pseudo_types nn$o pseudo_upf
R 4248 5 254 pseudo_types rcut pseudo_upf
R 4250 5 256 pseudo_types rcut$sd pseudo_upf
R 4251 5 257 pseudo_types rcut$p pseudo_upf
R 4252 5 258 pseudo_types rcut$o pseudo_upf
R 4254 5 260 pseudo_types rcutus pseudo_upf
R 4256 5 262 pseudo_types rcutus$sd pseudo_upf
R 4257 5 263 pseudo_types rcutus$p pseudo_upf
R 4258 5 264 pseudo_types rcutus$o pseudo_upf
R 4260 5 266 pseudo_types jchi pseudo_upf
R 4262 5 268 pseudo_types jchi$sd pseudo_upf
R 4263 5 269 pseudo_types jchi$p pseudo_upf
R 4264 5 270 pseudo_types jchi$o pseudo_upf
R 4266 5 272 pseudo_types jjj pseudo_upf
R 4268 5 274 pseudo_types jjj$sd pseudo_upf
R 4269 5 275 pseudo_types jjj$p pseudo_upf
R 4270 5 276 pseudo_types jjj$o pseudo_upf
R 4272 5 278 pseudo_types paw_data_format pseudo_upf
R 4273 5 279 pseudo_types tpawp pseudo_upf
R 4274 5 280 pseudo_types paw pseudo_upf
R 4275 5 281 pseudo_types grid pseudo_upf
R 4277 5 283 pseudo_types grid$p pseudo_upf
R 4279 5 285 pseudo_types has_gipaw pseudo_upf
R 4280 5 286 pseudo_types gipaw_data_format pseudo_upf
R 4281 5 287 pseudo_types gipaw_ncore_orbitals pseudo_upf
R 4282 5 288 pseudo_types gipaw_core_orbital_n pseudo_upf
R 4284 5 290 pseudo_types gipaw_core_orbital_n$sd pseudo_upf
R 4285 5 291 pseudo_types gipaw_core_orbital_n$p pseudo_upf
R 4286 5 292 pseudo_types gipaw_core_orbital_n$o pseudo_upf
R 4288 5 294 pseudo_types gipaw_core_orbital_l pseudo_upf
R 4290 5 296 pseudo_types gipaw_core_orbital_l$sd pseudo_upf
R 4291 5 297 pseudo_types gipaw_core_orbital_l$p pseudo_upf
R 4292 5 298 pseudo_types gipaw_core_orbital_l$o pseudo_upf
R 4294 5 300 pseudo_types gipaw_core_orbital_el pseudo_upf
R 4296 5 302 pseudo_types gipaw_core_orbital_el$sd pseudo_upf
R 4297 5 303 pseudo_types gipaw_core_orbital_el$p pseudo_upf
R 4298 5 304 pseudo_types gipaw_core_orbital_el$o pseudo_upf
R 4300 5 306 pseudo_types gipaw_core_orbital pseudo_upf
R 4303 5 309 pseudo_types gipaw_core_orbital$sd pseudo_upf
R 4304 5 310 pseudo_types gipaw_core_orbital$p pseudo_upf
R 4305 5 311 pseudo_types gipaw_core_orbital$o pseudo_upf
R 4307 5 313 pseudo_types gipaw_vlocal_ae pseudo_upf
R 4309 5 315 pseudo_types gipaw_vlocal_ae$sd pseudo_upf
R 4310 5 316 pseudo_types gipaw_vlocal_ae$p pseudo_upf
R 4311 5 317 pseudo_types gipaw_vlocal_ae$o pseudo_upf
R 4313 5 319 pseudo_types gipaw_vlocal_ps pseudo_upf
R 4315 5 321 pseudo_types gipaw_vlocal_ps$sd pseudo_upf
R 4316 5 322 pseudo_types gipaw_vlocal_ps$p pseudo_upf
R 4317 5 323 pseudo_types gipaw_vlocal_ps$o pseudo_upf
R 4319 5 325 pseudo_types gipaw_wfs_nchannels pseudo_upf
R 4320 5 326 pseudo_types gipaw_wfs_el pseudo_upf
R 4322 5 328 pseudo_types gipaw_wfs_el$sd pseudo_upf
R 4323 5 329 pseudo_types gipaw_wfs_el$p pseudo_upf
R 4324 5 330 pseudo_types gipaw_wfs_el$o pseudo_upf
R 4326 5 332 pseudo_types gipaw_wfs_ll pseudo_upf
R 4328 5 334 pseudo_types gipaw_wfs_ll$sd pseudo_upf
R 4329 5 335 pseudo_types gipaw_wfs_ll$p pseudo_upf
R 4330 5 336 pseudo_types gipaw_wfs_ll$o pseudo_upf
R 4332 5 338 pseudo_types gipaw_wfs_ae pseudo_upf
R 4335 5 341 pseudo_types gipaw_wfs_ae$sd pseudo_upf
R 4336 5 342 pseudo_types gipaw_wfs_ae$p pseudo_upf
R 4337 5 343 pseudo_types gipaw_wfs_ae$o pseudo_upf
R 4339 5 345 pseudo_types gipaw_wfs_rcut pseudo_upf
R 4341 5 347 pseudo_types gipaw_wfs_rcut$sd pseudo_upf
R 4342 5 348 pseudo_types gipaw_wfs_rcut$p pseudo_upf
R 4343 5 349 pseudo_types gipaw_wfs_rcut$o pseudo_upf
R 4345 5 351 pseudo_types gipaw_wfs_rcutus pseudo_upf
R 4347 5 353 pseudo_types gipaw_wfs_rcutus$sd pseudo_upf
R 4348 5 354 pseudo_types gipaw_wfs_rcutus$p pseudo_upf
R 4349 5 355 pseudo_types gipaw_wfs_rcutus$o pseudo_upf
R 4351 5 357 pseudo_types gipaw_wfs_ps pseudo_upf
R 4354 5 360 pseudo_types gipaw_wfs_ps$sd pseudo_upf
R 4355 5 361 pseudo_types gipaw_wfs_ps$p pseudo_upf
R 4356 5 362 pseudo_types gipaw_wfs_ps$o pseudo_upf
R 4410 25 18 scf scf_type
R 4411 5 19 scf of_r scf_type
R 4414 5 22 scf of_r$sd scf_type
R 4415 5 23 scf of_r$p scf_type
R 4416 5 24 scf of_r$o scf_type
R 4418 5 26 scf of_g scf_type
R 4421 5 29 scf of_g$sd scf_type
R 4422 5 30 scf of_g$p scf_type
R 4423 5 31 scf of_g$o scf_type
R 4425 5 33 scf kin_r scf_type
R 4428 5 36 scf kin_r$sd scf_type
R 4429 5 37 scf kin_r$p scf_type
R 4430 5 38 scf kin_r$o scf_type
R 4432 5 40 scf kin_g scf_type
R 4435 5 43 scf kin_g$sd scf_type
R 4436 5 44 scf kin_g$p scf_type
R 4437 5 45 scf kin_g$o scf_type
R 4439 5 47 scf ns scf_type
R 4444 5 52 scf ns$sd scf_type
R 4445 5 53 scf ns$p scf_type
R 4446 5 54 scf ns$o scf_type
R 4448 5 56 scf bec scf_type
R 4452 5 60 scf bec$sd scf_type
R 4453 5 61 scf bec$p scf_type
R 4454 5 62 scf bec$o scf_type
R 4456 25 64 scf mix_type
R 4459 5 67 scf of_g mix_type
R 4460 5 68 scf of_g$sd mix_type
R 4461 5 69 scf of_g$p mix_type
R 4462 5 70 scf of_g$o mix_type
R 4466 5 74 scf kin_g mix_type
R 4467 5 75 scf kin_g$sd mix_type
R 4468 5 76 scf kin_g$p mix_type
R 4469 5 77 scf kin_g$o mix_type
R 4475 5 83 scf ns mix_type
R 4476 5 84 scf ns$sd mix_type
R 4477 5 85 scf ns$p mix_type
R 4478 5 86 scf ns$o mix_type
R 4483 5 91 scf bec mix_type
R 4484 5 92 scf bec$sd mix_type
R 4485 5 93 scf bec$p mix_type
R 4486 5 94 scf bec$o mix_type
S 4626 23 5 0 0 0 4630 582 14607 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 write_rho_general
S 4627 1 3 1 0 1697 1 4626 14367 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rho
S 4628 1 3 1 0 6 1 4626 14454 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nspin
S 4629 1 3 1 0 28 1 4626 13173 80000014 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 extension
S 4630 14 5 0 0 0 1 4626 14607 10 400000 A 0 0 0 0 0 0 0 904 3 0 0 0 0 0 0 0 0 0 0 0 0 51 0 582 0 0 0 0 write_rho_general
F 4630 3 4627 4628 4629
S 4631 23 5 0 0 0 4635 582 14639 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_rho_general
S 4632 1 3 3 0 1557 1 4631 14367 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rho
S 4633 1 3 1 0 6 1 4631 14454 14 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nspin
S 4634 1 3 1 0 28 1 4631 13173 80000014 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 extension
S 4635 14 5 0 0 0 1 4631 14639 10 400000 A 0 0 0 0 0 0 0 908 3 0 0 0 0 0 0 0 0 0 0 0 0 93 0 582 0 0 0 0 read_rho_general
F 4635 3 4632 4633 4634
S 4636 23 5 0 0 0 4640 582 14592 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 write_rho_only
S 4637 7 3 1 0 1703 1 4636 14367 800214 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rho
S 4638 6 3 1 0 6 1 4636 14454 800014 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nspin
S 4639 1 3 1 0 28 1 4636 13173 80000014 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 extension
S 4640 14 5 0 0 0 1 4636 14592 210 400000 A 0 0 0 0 0 0 0 912 3 0 0 0 0 0 0 0 0 0 0 0 0 159 0 582 0 0 0 0 write_rho_only
F 4640 3 4637 4638 4639
S 4641 6 1 0 0 6 1 4636 25820 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3119
S 4642 6 1 0 0 6 1 4636 25829 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3120
S 4643 6 1 0 0 6 1 4636 25838 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3123
S 4644 6 1 0 0 6 1 4636 25847 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3125
S 4645 23 5 0 0 0 4649 582 14625 10 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 read_rho_only
S 4646 7 3 2 0 1706 1 4645 14367 800214 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 rho
S 4647 6 3 1 0 6 1 4645 14454 800014 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 nspin
S 4648 1 3 1 0 28 1 4645 13173 80000014 43000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 extension
S 4649 14 5 0 0 0 1 4645 14625 210 400000 A 0 0 0 0 0 0 0 916 3 0 0 0 0 0 0 0 0 0 0 0 0 243 0 582 0 0 0 0 read_rho_only
F 4649 3 4646 4647 4648
S 4650 6 1 0 0 6 1 4645 25856 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_2806
S 4651 6 1 0 0 6 1 4645 25847 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3125
S 4652 6 1 0 0 6 1 4645 25865 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3128
S 4653 6 1 0 0 6 1 4645 25874 40800016 3000 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 z_e_3130
A 2574 2 0 0 1513 1246 3994 0 0 0 2574 0 0 0 0 0 0 0 0 0
A 2805 1 0 0 1491 6 4644 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2806 1 0 0 1516 6 3677 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2807 1 0 0 2541 6 4641 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2808 1 0 0 1487 6 4638 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2809 1 0 0 1485 6 4642 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2810 1 0 0 1488 6 4643 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2811 1 0 0 2537 6 4653 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2812 1 0 0 2658 6 4650 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2813 1 0 0 1634 6 4647 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2814 1 0 0 2664 6 4651 0 0 0 0 0 0 0 0 0 0 0 0 0
A 2815 1 0 0 0 6 4652 0 0 0 0 0 0 0 0 0 0 0 0 0
Z
T 4045 1240 0 3 0 0
A 4046 1246 0 0 1 2574 1
A 4047 1246 0 0 1 2574 1
A 4048 1246 0 0 1 2574 1
A 4049 1246 0 0 1 2574 1
A 4050 1246 0 0 1 2574 1
A 4051 1246 0 0 1 2574 1
A 4052 1246 0 0 1 2574 0
T 4410 1557 0 0 0 0
A 4415 7 1599 0 1 2 1
A 4422 7 1601 0 1 2 1
A 4429 7 1603 0 1 2 1
A 4436 7 1605 0 1 2 1
A 4445 7 1607 0 1 2 1
A 4453 7 1609 0 1 2 0
T 4456 1611 0 0 0 0
A 4461 7 1641 0 1 2 1
A 4468 7 1643 0 1 2 1
A 4477 7 1645 0 1 2 1
A 4485 7 1647 0 1 2 0
Z
