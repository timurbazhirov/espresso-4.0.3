V26 pwcom
9 pwcom.F90 S582 0
04/24/2012  15:53:20
use uspp public 0 direct
use basis public 0 direct
use gvect public 0 direct
use gsmooth public 0 direct
use klist public 0 direct
use lsda_mod public 0 direct
use ktetra public 0 direct
use symme public 0 direct
use rap_point_group public 0 direct
use rap_point_group_so public 0 direct
use rap_point_group_is public 0 direct
use vlocal public 0 direct
use wvfct public 0 direct
use ener public 0 direct
use force_mod public 0 direct
use relax public 0 direct
use cellmd public 0 direct
use char public 0 direct
use us public 0 direct
use ldau public 0 direct
use extfield public 0 direct
use bp public 0 direct
use fixed_occ public 0 direct
use spin_orb public 0 direct
use cell_base private
use constants private
use cell_base private
use constants private
enduse
D 119 21 9 1 3 3 0 0 0 0 0
 0 3 3 3 3 3
D 122 21 6 1 3 128 0 0 0 0 0
 0 128 3 3 128 128
D 125 21 9 1 3 3 0 0 0 0 0
 0 3 3 3 3 3
D 131 21 9 1 3 3 0 0 0 0 0
 0 3 3 3 3 3
D 134 21 6 1 3 128 0 0 0 0 0
 0 128 3 3 128 128
D 137 21 9 1 3 3 0 0 0 0 0
 0 3 3 3 3 3
S 582 24 0 0 0 9 1 0 4658 10005 0 A 0 0 0 0 0 0 0 0 0 0 0 0 613 0 0 0 0 0 0 0 0 580 0 0 0 0 0 0 pwcom
S 584 23 0 0 0 9 717 582 4674 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 e2
S 585 23 0 0 0 9 696 582 4677 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 rytoev
S 586 23 0 0 0 9 719 582 4684 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 amconv
S 587 23 0 0 0 9 677 582 4691 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 pi
S 588 23 0 0 0 9 678 582 4694 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 tpi
S 589 23 0 0 0 9 679 582 4698 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 fpi
S 591 23 0 0 0 9 746 582 4712 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 celldm
S 592 23 0 0 0 9 757 582 4719 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 at
S 593 23 0 0 0 9 764 582 4722 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 bg
S 594 23 0 0 0 9 745 582 4725 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 alat
S 595 23 0 0 0 9 754 582 4730 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 omega
S 596 23 0 0 0 9 755 582 4736 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 tpiba
S 597 23 0 0 0 9 756 582 4742 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 tpiba2
S 598 23 0 0 0 6 771 582 4749 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 ibrav
S 599 23 0 0 0 9 772 582 4755 4 0 A 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 582 0 0 0 0 symm_type
R 677 16 2 constants pi
R 678 16 3 constants tpi
R 679 16 4 constants fpi
R 696 16 21 constants rytoev
R 717 16 42 constants e2
R 719 16 44 constants amconv
S 727 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
S 730 3 0 0 0 6 1 1 0 0 0 A 0 0 0 0 0 0 0 0 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 6
R 745 6 15 cell_base alat
R 746 7 16 cell_base celldm
R 754 6 24 cell_base omega
R 755 6 25 cell_base tpiba
R 756 6 26 cell_base tpiba2
R 757 7 27 cell_base at
R 759 7 29 cell_base z_c_0$ac
R 761 7 31 cell_base z_c_1$ac
R 763 7 33 cell_base z_c_2$ac
R 764 7 34 cell_base bg
R 766 7 36 cell_base z_c_3$ac
R 768 7 38 cell_base z_c_4$ac
R 770 7 40 cell_base z_c_5$ac
R 771 6 41 cell_base ibrav
R 772 6 42 cell_base symm_type
A 110 2 0 0 0 6 727 0 0 0 110 0 0 0 0 0 0 0 0 0
A 114 2 0 0 40 9 577 0 0 0 114 0 0 0 0 0 0 0 0 0
A 128 2 0 0 0 6 730 0 0 0 128 0 0 0 0 0 0 0 0 0
A 204 1 0 1 0 122 759 0 0 0 0 0 0 0 0 0 0 0 0 0
A 207 1 0 3 0 125 761 0 0 0 0 0 0 0 0 0 0 0 0 0
A 210 1 0 3 0 119 763 0 0 0 0 0 0 0 0 0 0 0 0 0
A 214 1 0 1 0 134 766 0 0 0 0 0 0 0 0 0 0 0 0 0
A 217 1 0 3 0 137 768 0 0 0 0 0 0 0 0 0 0 0 0 0
A 220 1 0 3 0 131 770 0 0 0 0 0 0 0 0 0 0 0 0 0
Z
J 78 1 1
V 204 122 7 0
R 0 122 0 0
A 0 6 0 0 1 110 1
A 0 6 0 0 1 110 0
J 78 1 1
V 207 125 7 0
R 0 125 0 0
A 0 9 0 0 1 114 0
J 78 1 1
V 210 119 7 0
R 0 119 0 0
A 0 9 0 0 1 114 0
J 79 1 1
V 214 134 7 0
R 0 134 0 0
A 0 6 0 0 1 110 1
A 0 6 0 0 1 110 0
J 79 1 1
V 217 137 7 0
R 0 137 0 0
A 0 9 0 0 1 114 0
J 79 1 1
V 220 131 7 0
R 0 131 0 0
A 0 9 0 0 1 114 0
Z
