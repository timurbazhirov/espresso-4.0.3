

! Input/Output Tool Kit (IOTK)
! Copyright (C) 2004-2006 Giovanni Bussi
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

!------------------------------------------------------------------------------!
! Inclusion of configuration file
! Input/Output Tool Kit (IOTK)
! Copyright (C) 2004 Giovanni Bussi
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
!
!------------------------------------------------------------------------------!
! CONFIGURATION FILE FOR IOTK 1.1.0 for Quantum-Espresso
!------------------------------------------------------------------------------!
! The following lines map some commonly defined system macro to the internal
! iotk macros.
! Iotk macros which are not defined take their default values.
! See the manual for a list of iotk macros.


! Generic options valid for quantum-espresso
! QE uses ranks up to four and default integer/logicals only


! some compilers do not like the following
!    #define __IOTK_REAL1 selected_real_kind(6,30)
!    #define __IOTK_REAL2 selected_real_kind(14,200)
! so we use explicit kinds
! Machine-dependent options
! Only for compilers that require some special tricks






    !
    ! proceed with a machine dependent def where available
    !





!------------------------------------------------------------------------------!



! The macros are defined with -D option or inside iotk_config.h
! The default values are set here

! Maximum rank of an array

! Minimum value used in iotk_free_unit

! Maximum value used in iotk_free_unit

! Unit for errors

! Unit for output

! Kind for header in binary files

! Maximum number of arguments to the iotk tool

! Character (or eventually string) for newline
! It may be adjusted for particular systems
! It is used only in binary files, surrounding the tags so that they can
! be easily isolated with grep
! Unix    achar(10)
! Mac-OS  achar(13)
! Windows ? (now it should be a single byte)

! Character for EOS

! These are the default kinds, which depend on the options used
! during the library compilation
! Only default characters are implemented
! For logical, integer and real types, the c precompiler
! looks for defined kinds. If no kind is found, the default
! is used as __IOTK_type1

! Complex are treated indentically to reals
! These lines map the definitions.

! Some useful check follow









module iotk_scan_interf
implicit none
private

public :: iotk_scan_begin
public :: iotk_scan_end
public :: iotk_scan_pi
public :: iotk_scan_empty
public :: iotk_scan_tag
public :: iotk_scan
public :: iotk_getline

interface iotk_scan_begin
subroutine iotk_scan_begin_x(unit,name,attr,dummy,found,ierr)
  use iotk_base
  implicit none
  integer,                        intent(in)  :: unit
  character(len=*),               intent(in)  :: name
  character(len=*),     optional, intent(out) :: attr
  type(iotk_dummytype), optional              :: dummy
  logical,              optional, intent(out) :: found
  integer,              optional, intent(out) :: ierr
end subroutine iotk_scan_begin_x
end interface

interface iotk_scan_end
subroutine iotk_scan_end_x(unit,name,dummy,ierr)
  use iotk_base
  implicit none
  integer,                intent(in)  :: unit
  character(*),           intent(in)  :: name
  type(iotk_dummytype), optional      :: dummy
  integer,      optional, intent(out) :: ierr
end subroutine iotk_scan_end_x
end interface

interface iotk_scan_pi
subroutine iotk_scan_pi_x(unit,name,attr,dummy,found,ierr)
  use iotk_base
  implicit none
  integer,                        intent(in)  :: unit
  character(*),                   intent(in)  :: name
  character(len=*),     optional, intent(out) :: attr
  type(iotk_dummytype), optional              :: dummy
  logical,              optional, intent(out) :: found
  integer,              optional, intent(out) :: ierr
end subroutine iotk_scan_pi_x
end interface

interface iotk_scan_empty
subroutine iotk_scan_empty_x(unit,name,attr,dummy,found,ierr)
  use iotk_base
  implicit none
  integer,                        intent(in)  :: unit
  character(len=*),               intent(in)  :: name
  character(len=*),     optional, intent(out) :: attr
  type(iotk_dummytype), optional              :: dummy
  logical,              optional, intent(out) :: found
  integer,              optional, intent(out) :: ierr
end subroutine iotk_scan_empty_x
end interface

interface iotk_scan_tag
subroutine iotk_scan_tag_x(unit,direction,control,tag,binary,stream,ierr)
  use iotk_base
  implicit none
  integer,                 intent(in)  :: unit
  integer,                 intent(in)  :: direction
  integer,                 intent(out) :: control
  character(iotk_taglenx), intent(out) :: tag
  logical,                 intent(in)  :: binary
  logical,                 intent(in)  :: stream
  integer,                 intent(out) :: ierr
end subroutine iotk_scan_tag_x
end interface

interface iotk_scan
subroutine iotk_scan_x(unit,direction,control,name,attr,binary,stream,found,ierr)
  use iotk_base
  implicit none
  integer,                 intent(in)  :: unit
  integer,                 intent(in)  :: direction
  integer,                 intent(in)  :: control
  character(iotk_namlenx), intent(in)  :: name
  character(iotk_attlenx), intent(out) :: attr
  logical,                 intent(in)  :: binary
  logical,                 intent(in)  :: stream
  logical,                 intent(out) :: found
  integer,                 intent(out) :: ierr
end subroutine iotk_scan_x
end interface

interface iotk_getline
subroutine iotk_getline_x(unit,line,length,ierr)
  implicit none
  integer,           intent(in)  :: unit
  character(len=*),  intent(out) :: line
  integer, optional, intent(out) :: length
  integer, optional, intent(out) :: ierr
end subroutine iotk_getline_x
end interface

end module iotk_scan_interf
